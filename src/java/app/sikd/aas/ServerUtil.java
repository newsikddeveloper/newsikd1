package app.sikd.aas;

import app.sikd.util.PropertiesLoader;
import app.sikd.web.LoginMBean;
//import app.sikd.web.UserMBean;
import java.io.Serializable;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.el.ValueExpression;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.naming.Context;
import javax.naming.InitialContext;

/**
 *
 * @author sora
 */
public class ServerUtil implements Serializable {

    public static Context getLoginContext() throws Exception {
        Properties pr = PropertiesLoader.loadProperties("sikd.properties");
        Properties prop = new Properties();
        if (pr.getProperty("aasurl") != null && !pr.getProperty("aasurl").trim().equals("")) {
            prop.put(InitialContext.PROVIDER_URL, pr.getProperty("aasurl"));            
        }
        
        Context ctx = new InitialContext(prop);
        return ctx;
    }
    public static Context getOfficeContext() throws Exception {
        Properties pr = PropertiesLoader.loadProperties("sikd.properties");
        Properties prop = new Properties();
        if (pr.getProperty("officeurl") != null && !pr.getProperty("officeurl").trim().equals("")) {
            prop.put(InitialContext.PROVIDER_URL, pr.getProperty("officeurl"));
        }

        Context ctx = new InitialContext(prop);
        return ctx;
    }
    
    public static String getDomain(){
        try {
            Properties pr = PropertiesLoader.loadProperties("sikd.properties");
            
            return pr.getProperty("sikddomain");
        } catch (Exception ex) {
            Logger.getLogger(ServerUtil.class.getName()).log(Level.SEVERE, null, ex);
            return "";
        }
    }
    
    public static boolean cekSession(){
        FacesContext fc1 = FacesContext.getCurrentInstance();
            ExternalContext excontext1 = fc1.getExternalContext();
        if (excontext1.getSession(false) == null ) {
            return false;
        }
        return true;
    }    
    
    public static LoginMBean getLoginMBean(){
        FacesContext facesContext = FacesContext.getCurrentInstance();
            ValueExpression valExp = facesContext.getApplication().getExpressionFactory().createValueExpression(facesContext.getELContext(), "#{userBean}", LoginMBean.class);
            LoginMBean userbean = (LoginMBean) valExp.getValue(facesContext.getELContext());
            return userbean;
    }

}
