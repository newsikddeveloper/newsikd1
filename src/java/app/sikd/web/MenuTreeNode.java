/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web;

import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

/**
 *
 * @author sora
 */
public class MenuTreeNode extends DefaultTreeNode{
    private String addressPage;
    private String addressBar;
    public MenuTreeNode() {
    }

    public MenuTreeNode(Object data) {
        super(data);
    }

    public MenuTreeNode(Object data, TreeNode parent) {
        super(data, parent);
    }

    public MenuTreeNode(String type, Object data, TreeNode parent) {
        super(type, data, parent);
    }
    public MenuTreeNode(String type, Object data, String addressPage, String addressBar, TreeNode parent) {
        super(type, data, parent);
        this.addressPage = addressPage;
        this.addressBar = addressBar;
    }

    public String getAddressBar() {
        return addressBar;
    }

    public void setAddressBar(String addressBar) {
        this.addressBar = addressBar;
    }

    public String getAddressPage() {
        return addressPage;
    }

    public void setAddressPage(String addressPage) {
        this.addressPage = addressPage;
    }
    
    
    
}
