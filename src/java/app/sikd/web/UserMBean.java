/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web;

import app.sikd.entity.mgr.TUserAccount;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author sora
 */
//@Named(value = "userMBean")
@ManagedBean(name="userMBean")
@SessionScoped
public class UserMBean implements Serializable{
    private TUserAccount userAccount;
    
    public void initData(TUserAccount userAccount){
        this.userAccount = userAccount;
    }

    public TUserAccount getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(TUserAccount userAccount) {
        this.userAccount = userAccount;
    }
}
