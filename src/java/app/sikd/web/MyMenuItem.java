package app.sikd.web;

import org.primefaces.model.menu.DefaultMenuItem;

/**
 *
 * @author sora
 */
public class MyMenuItem extends DefaultMenuItem{
    private boolean write;
    public MyMenuItem(Object value) {
        super(value);
    }

    public boolean isWrite() {
        return write;
    }

    public void setWrite(boolean write) {
        this.write = write;
    }
}
