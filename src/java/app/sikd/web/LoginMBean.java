package app.sikd.web;

import app.sikd.aas.ServerUtil;
import app.sikd.aas.SessionUtil;
import app.sikd.entity.mgr.TUserAccount;
import app.sikd.login.ejb.session.NewLoginSessionBeanRemote;
import app.sikd.util.SIKDUtil;
import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.el.ValueExpression;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;

///**
// *
// * @author detra
// */
//@ManagedBean
//@SessionScoped
/**
 *
 * @author sora
 */
@ManagedBean(name = "loginMBean")
@SessionScoped
public class LoginMBean implements Serializable {
    NewLoginSessionBeanRemote loginSession;
    String name;
    String strPass;
    private String message;

    String domain;

    @PostConstruct
    public void init() {
        try {
            ExternalContext excontextf = FacesContext.getCurrentInstance().getExternalContext();
            excontextf.invalidateSession();

            Context ctxLogin = ServerUtil.getLoginContext();
//            Context ctxOffice = ServerUtil.getOfficeContext();

            loginSession = (NewLoginSessionBeanRemote) ctxLogin.lookup("NewLoginSessionBean/remote");
            domain = ServerUtil.getDomain();
            message = "";
        } catch (NamingException ex) {
            Logger.getLogger(LoginMBean.class.getName()).log(Level.SEVERE, null, ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Kegagalan", "Gagal Inisialisasi." + ex.toString()));
        } catch (Exception ex) {
            Logger.getLogger(LoginMBean.class.getName()).log(Level.SEVERE, null, ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Kegagalan", "Gagal Inisialisasi." + ex.toString()));
        }
    }

    void getPassword() throws Exception {
        if (strPass == null || strPass.trim().equals("")) {
            throw new Exception("Silahkan isi Password");
        }
    }

    public void onLogin() {
        try {
            if (name == null) {
                name = "";
            }
            getPassword();
            byte[] p = new byte[strPass.trim().length()];
            for (int i = 0; i < p.length; i++) {
                p[i] = (byte) strPass.trim().charAt(i);
            }
            TUserAccount user = loginSession.login(name, p);
            FacesContext facesContext = FacesContext.getCurrentInstance();
            SessionUtil.setUsernameCookie(name);
            SessionUtil.setUserGroupCookie(String.valueOf(user.getGroup().getId()));
            
            ExternalContext excontext = facesContext.getExternalContext();
            HttpServletRequest req = (HttpServletRequest)excontext.getRequest();
            req.getSession().setAttribute(SIKDUtil.SESSION_USE, user);
            
            ValueExpression valExp2 = facesContext.getApplication().getExpressionFactory().createValueExpression(facesContext.getELContext(), "#{menuMBean}", MenuMBean.class);
            MenuMBean menubean = (MenuMBean) valExp2.getValue(facesContext.getELContext());
            menubean.setUserGroup(user.getGroup());
            menubean.createMenu(user.getMenus());
            
            String dom = ServerUtil.getDomain();
            excontext.redirect(dom + excontext.getRequestContextPath() + "/home.jsf");
            
        } catch (Exception ex) {
            Logger.getLogger(LoginMBean.class.getName()).log(Level.SEVERE, null, ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Error: ", ex.getMessage()));
            message = ex.getMessage();
        }
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStrPass() {
        return strPass;
    }

    public void setStrPass(String strPass) {
        this.strPass = strPass;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    
    public void logout(){
        try {
            FacesContext fc = FacesContext.getCurrentInstance();
            ExternalContext excontext = fc.getExternalContext();            
            String dom = ServerUtil.getDomain();
            excontext.redirect(dom + excontext.getRequestContextPath()+ "/index.jsf");//nanti ambil ini dari properties
            SessionUtil.deleteCookies();
            SessionUtil.deleteServletSession();
//            SessionUtil.setTUserAccount(null);
        } catch (IOException ex) {
//            Logger.getLogger(LoginMBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void goLoginFromNoSession(){
        try {
            SessionUtil.deleteCookies();
            String dom = ServerUtil.getDomain();
            
            FacesContext fc = FacesContext.getCurrentInstance();
            ExternalContext excontext = fc.getExternalContext();
            
            excontext.redirect(dom + excontext.getRequestContextPath()+ "/index.jsf");//nanti ambil ini dari properties

            SessionUtil.deleteServletSession();
        } catch (IOException ex) {
            Logger.getLogger(LoginMBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

}
