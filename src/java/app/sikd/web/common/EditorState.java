/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.web.common;

/**
 *
 * @author Dell
 */
public enum EditorState {

    /**
     * mode add new data
     *
     */
    add("EditorState::ADD"),
    
    /**
     * mode modification exiting data
     *
     */
    edit("EditorState::EDIT");
    private String internalRepresenation;

    private EditorState(String code) {
        this.internalRepresenation = code;
    }

    @Override
    public String toString() {
        return internalRepresenation;
    }
}
