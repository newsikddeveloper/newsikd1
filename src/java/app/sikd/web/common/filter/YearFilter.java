/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.common.filter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Nongky
 */
public class YearFilter implements Serializable{

    public List<String> getYears() {
        List<String> years = new ArrayList<>();
        for (int i = 2011; i < 2021; i++) {
            years.add(String.valueOf(i));
        }
        return years;
    }

}
