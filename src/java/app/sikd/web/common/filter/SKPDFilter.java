/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.common.filter;

import app.sikd.aas.ServerUtil;
import app.sikd.office.ejb.session.APBDSessionBeanRemote;
import app.sikd.office.ejb.session.LaporanSessionBeanRemote;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;

/**
 *
 * @author Nongky
 */
public class SKPDFilter {

    private APBDSessionBeanRemote service;

    public SKPDFilter() {
        try {
            Context context = ServerUtil.getOfficeContext();
            service = (APBDSessionBeanRemote) context.lookup("APBDSessionBean/remote");
        } catch (Exception ex) {
            Logger.getLogger(SKPDFilter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<String> getSKPDs(short _year, String _kodeSatker, short _dataCode, short _coaType) throws Exception {
        return service.getDinasKirimAPBD(_year, _kodeSatker, _dataCode, _coaType);
    }

}
