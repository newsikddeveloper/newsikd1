/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.common.filter;

import app.sikd.aas.ServerUtil;
import app.sikd.entity.Pemda;
import app.sikd.login.ejb.session.AdministrasiSessionBeanRemote;
import java.io.Serializable;
import java.util.List;
import javax.naming.Context;

/**
 *
 * @author Nongky
 */
public class PemdaFilter implements Serializable{

    private AdministrasiSessionBeanRemote service;
    private List<Pemda> pemdas;

    public PemdaFilter() {
        try {
            Context ctxLogin = ServerUtil.getLoginContext();
            service = (AdministrasiSessionBeanRemote) ctxLogin.lookup("AdministrasiSessionBean/remote");
            pemdas = service.getPemdas();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public List<Pemda> getPemdas() {
        return pemdas;
    }

}
