/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.web.common.filter;

import app.sikd.aas.ServerUtil;
import app.sikd.entity.mgr.TUserGroup;
import app.sikd.login.ejb.session.AdministrasiSessionBeanRemote;
import java.util.List;
import javax.naming.Context;

/**
 *
 * @author Dell
 */
public class UserGroupFilter {
    private AdministrasiSessionBeanRemote service;
    private List<TUserGroup> userGroups;

    public UserGroupFilter() {
        try {
            Context ctxLogin = ServerUtil.getLoginContext();
            service = (AdministrasiSessionBeanRemote) ctxLogin.lookup("AdministrasiSessionBean/remote");
            userGroups = service.getInquiryUserGroup(null);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public List<TUserGroup> getUserGroups() {
        return userGroups;
    }
}
