/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.common;

import java.io.Serializable;

/**
 *
 * @author Nongky
 */
public abstract class ASinglePageBackbean implements Serializable {

    private String page = "";

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }
}
