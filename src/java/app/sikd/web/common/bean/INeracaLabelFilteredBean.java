/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.common.bean;

import java.util.List;

/**
 *
 * @author Nongky
 */
public interface INeracaLabelFilteredBean {

    public List<String> getNeracaLabels();

    public void setSelectedNeracaLabel(String _label);

    public String getSelectedNeracaLabel();

}
