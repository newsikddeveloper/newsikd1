/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.common.bean;

import java.util.List;

/**
 *
 * @author Nongky
 */
public interface IArusKasLabelFilteredBean {

    public List<String> getArusKasLabels();

    public void setSelectedArusKasLabel(String _label);

    public String getSelectedArusKasLabel();

}
