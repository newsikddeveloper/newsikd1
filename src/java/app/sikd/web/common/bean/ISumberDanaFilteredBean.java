/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.common.bean;

import java.io.Serializable;

/**
 *
 * @author Nongky
 */
public interface ISumberDanaFilteredBean extends Serializable {

    public String getSumberDana();

    public void setSumberDana(String _sumberDana);

}
