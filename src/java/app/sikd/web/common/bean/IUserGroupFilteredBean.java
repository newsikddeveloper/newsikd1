/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.web.common.bean;

import app.sikd.entity.mgr.TUserGroup;
import java.util.List;

/**
 *
 * @author Dell
 */
public interface IUserGroupFilteredBean {
    public List<TUserGroup> getUserGroups();

    public void setSelectedUserGroup(TUserGroup userGroup);

    /**
     *
     * @return
     */
    public TUserGroup getSelectedUserGroup();
}
