/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.common.bean;

import app.sikd.web.common.filter.NeracaLabel;
import app.sikd.web.common.filter.SemesterFilter;
import app.sikd.web.laporan.lkpd.NeracaBackBean;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Nongky
 */
public abstract class ANeracaBackBean extends ABasicFilteredBackBean implements ISemesterFilteredBean, INeracaLabelFilteredBean {

    private SemesterFilter semesterFilter;
    private NeracaLabel neracaLabel;

    protected String semester;
    protected String label;

    @Override
    protected void init() {
        super.init();
        additionalRender = "data-filter-form:neraca-label-som";
        semesterFilter = new SemesterFilter();
        neracaLabel = new NeracaLabel();
    }

    @Override
    public List<String> getSemesters() {
        return semesterFilter.getSemesters();
    }

    @Override
    public void setSelectedSemester(String _semester) {
        semester = _semester;
    }

    @Override
    public String getSelectedSemester() {
        return semester;
    }

    private List<String> neracaLabels;

    @Override
    public List<String> getNeracaLabels() {
        return neracaLabels;
    }

    @Override
    public void setSelectedNeracaLabel(String _label) {
        label = _label;
    }

    @Override
    public String getSelectedNeracaLabel() {
        return label;
    }

    @Override
    public void onFilterChanged() {
        if (label != null && label.length() > 0) {
            onChangeProcess();
        } else {
            if (pemda != null
                    && (year != null && year.length() > 0)
                    && (semester != null && semester.length() > 0)) {
                try {
                    neracaLabels = neracaLabel.getNeracaLabels(new Short(year), new Short(semester), pemda.getKodeSatker());
                } catch (Exception ex) {
                    Logger.getLogger(NeracaBackBean.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
}
