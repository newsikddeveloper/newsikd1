/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.common.lov;

import app.sikd.aas.ServerUtil;
import app.sikd.entity.Pemda;
import app.sikd.login.ejb.session.AdministrasiSessionBeanRemote;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.naming.Context;

/**
 *
 * @author Nongky
 */
@ManagedBean(name = "PemdaLoV")
@ViewScoped
public class PemdaLoVBean implements Serializable {

    protected AdministrasiSessionBeanRemote service;

    private List<Pemda> pemdas;
    

    @PostConstruct
    private void init() {
        try {
            Context ctxLogin = ServerUtil.getLoginContext();
            service = (AdministrasiSessionBeanRemote) ctxLogin.lookup("AdministrasiSessionBean/remote");
            pemdas = service.getPemdas();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public List<Pemda> getPemdas() {
        if (pemdas == null) {
            pemdas = new ArrayList<>();
        }
        return pemdas;
    }

}
