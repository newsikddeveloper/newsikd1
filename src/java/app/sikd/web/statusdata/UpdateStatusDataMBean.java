package app.sikd.web.statusdata;

import app.sikd.aas.ServerUtil;
import app.sikd.aas.SessionUtil;
import app.sikd.entity.Pemda;
import app.sikd.entity.WilayahKerja;
import app.sikd.entity.backoffice.StatusPemda;
import app.sikd.entity.backoffice.setting.StatusData;
import app.sikd.entity.mgr.TUserAccount;
import app.sikd.login.ejb.session.AdministrasiSessionBeanRemote;
import app.sikd.login.ejb.session.NewLoginSessionBeanRemote;
import app.sikd.office.ejb.newsession.NewSettingSessionBeanRemote;
import app.sikd.office.ejb.newsession.NewVerifikasiSessionBeanRemote;
import app.sikd.office.ejb.session.APBDSessionBeanRemote;
import app.sikd.util.SIKDUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.naming.Context;
import javax.servlet.http.HttpServletRequest;
import org.primefaces.event.RowEditEvent;

/**
 *
 * @author PC04
 */
@ManagedBean(name = "updateStatusDataMBean")
@ViewScoped

public class UpdateStatusDataMBean implements Serializable {
    List<String> years;
    String selectedYear;
    String labelYear;
    List<Pemda> pemdas;
    Pemda selectedPemda;

    AdministrasiSessionBeanRemote administrasiSession;
    APBDSessionBeanRemote apbdSession;
    NewLoginSessionBeanRemote loginSession;
    NewVerifikasiSessionBeanRemote verifikasiSession;
    NewSettingSessionBeanRemote settingSession;
    long grupId;
    
    List<StatusPemda> statusPemdas;
    boolean viewPemda;
    private List<StatusData> statusDatas;
    
    
    @PostConstruct
    public void init() {
        try {
            labelYear = "";
            String username = SessionUtil.getCookieUsername();
            FacesContext context = FacesContext.getCurrentInstance();
            ExternalContext excontext = context.getExternalContext();
            HttpServletRequest httpServletRequest = (HttpServletRequest) excontext.getRequest();
            String path = httpServletRequest.getRequestURI();
            String namamenu = SIKDUtil.getNamaMenu(path);
            if (username == null) {
                throw new Exception("Identitas pengguna tidak bisa ditemukan atau sudah lewat masa sesi koneksi. "
                        + "Pengguna harus kembali melakukan login.");
            }
            String grupids = SessionUtil.getCookieGroupid();
            if (grupids != null) {
                grupId = Long.valueOf(grupids);
            }

            Context ctxLogin = ServerUtil.getLoginContext();
            Context ctxOffice = ServerUtil.getOfficeContext();
            administrasiSession = (AdministrasiSessionBeanRemote) ctxLogin.lookup("AdministrasiSessionBean/remote");
            loginSession = (NewLoginSessionBeanRemote) ctxLogin.lookup("NewLoginSessionBean/remote");
            verifikasiSession = (NewVerifikasiSessionBeanRemote) ctxOffice.lookup("NewVerifikasiSessionBean/remote");
            settingSession = (NewSettingSessionBeanRemote) ctxOffice.lookup("NewSettingSessionBean/remote");

            years = new ArrayList();
            statusPemdas = new ArrayList();
            pemdas = new ArrayList();
            if (loginSession.canRead(grupId, namamenu)) {
                years = SIKDUtil.getYears((short) 2010);
                TUserAccount user = loginSession.getUserbyName(username);
                statusDatas = settingSession.getStatusDatasByUserGroup(grupId);
                WilayahKerja wilker = user.getWilayah();
                if(wilker!=null && wilker.getPemdas()!=null){
                    pemdas = wilker.getPemdas();
                    viewPemda=true;
                }
                else{
                    viewPemda=false;
                    if(user.getPemda()!=null)
                        selectedPemda = user.getPemda();
                }
//                    pemdas = administrasiSession.getPemdas();
            }
        } catch (Exception ex) {
            System.out.println("Error bro:" + ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Kegagalan",
                    "Gagal dalam mengambil data User Group. "
                    + ex.toString()));
        }

    }

    public String getSelectedYear() {
        return selectedYear;
    }

    public void setSelectedYear(String selectedYear) {
        this.selectedYear = selectedYear;
    }

    public void onYearChange() {
        labelYear = ""; 
        statusPemdas.clear();
        if (selectedYear != null && !selectedYear.trim().equals("") && selectedPemda!=null && selectedPemda.getKodeSatker()!=null  && !selectedPemda.getKodeSatker().trim().equals("")) {
            try {
                labelYear = "TAHUN ANGGARAN " + selectedYear;                
                List<Pemda> pems = new ArrayList();
                pems.add(selectedPemda);
                
                statusPemdas=verifikasiSession.getStatusDataByPemda(pems, Short.valueOf(selectedYear));
//                for (StatusPemda stat: statusPemdas) {
//                    
//                    
//                }
            } catch (Exception ex) {
                Logger.getLogger(UpdateStatusDataMBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public List<String> getYears() {
        return years;
    }

    public String getLabelYear() {
        return labelYear;
    }

    public void setLabelYear(String labelYear) {
        this.labelYear = labelYear;
    }

    public List<StatusPemda> getStatusPemdas() {
        return statusPemdas;
    }

    public void setStatusPemdas(List<StatusPemda> statusPemdas) {
        this.statusPemdas = statusPemdas;
    }

    public List<Pemda> getPemdas() {
        return pemdas;
    }

    public void setPemdas(List<Pemda> pemdas) {
        this.pemdas = pemdas;
    }

    public Pemda getSelectedPemda() {
        return selectedPemda;
    }

    public void setSelectedPemda(Pemda selectedPemda) {
        this.selectedPemda = selectedPemda;
    }

    public boolean isViewPemda() {
        return viewPemda;
    }

    public void setViewPemda(boolean viewPemda) {
        this.viewPemda = viewPemda;
    }

    public List<StatusData> getStatusDatas() {
        return statusDatas;
    }

    public void setStatusDatas(List<StatusData> statusDatas) {
        this.statusDatas = statusDatas;
    }
    
    public void onEdit(RowEditEvent event) {
        try {
            if( event.getObject() instanceof StatusPemda){
                StatusPemda sp = (StatusPemda) event.getObject();
                String[] ikds = {"APBD Anggaran", "APBD Perubahan", 
        "Laporan Realisasi APBD Periode 1", "Laporan Realisasi APBD Periode 2",
        "Laporan Realisasi APBD Periode 3", "Laporan Realisasi APBD Periode 4",
        "Laporan Realisasi APBD Periode 5", "Laporan Realisasi APBD Periode 6",
        "Laporan Realisasi APBD Periode 7", "Laporan Realisasi APBD Periode 8",
        "Laporan Realisasi APBD Periode 9", "Laporan Realisasi APBD Periode 10",
        "Laporan Realisasi APBD Periode 11", "Laporan Realisasi APBD Periode 12",
        "LRA Pertanggungjawaban",
        "Neraca Semester 1", "Neraca Semester 2",
        "Arus Kas",
        "Laporan Operasional Triwulan 1", "Laporan Operasional Triwulan 2", "Laporan Operasional Triwulan 3",
        "Laporan Perubahan Saldo", "Laporan Perubahan Ekuitas",
        "DTH Periode 1", "DTH Periode 2", "DTH Periode 3",
        "DTH Periode 4", "DTH Periode 5", "DTH Periode 6",
        "DTH Periode 7", "DTH Periode 8", "DTH Periode 9",
        "DTH Periode 10", "DTH Periode 11", "DTH Periode 12",
        "Pinjaman Daerah", "Perhitungan Fihak Ketiga"};
                if(sp!=null && sp.getIkd()!=null){                    
                    if(sp.getIkd().trim().equals("APBD Anggaran")) verifikasiSession.updateStatusDataApbdByPemda(sp, Short.valueOf(getSelectedYear()), (short)0);
                    else if(sp.getIkd().trim().equals("APBD Perubahan")) verifikasiSession.updateStatusDataApbdByPemda(sp, Short.valueOf(getSelectedYear()), (short)1);
                    else if(sp.getIkd().trim().startsWith("Laporan Realisasi APBD Periode")) {
                        short per = Short.valueOf(sp.getIkd().trim().substring(sp.getIkd().trim().length()-1, sp.getIkd().trim().length()));
//                        verifikasiSession.updateStatusDataLraByPemda(sp, Short.valueOf(getSelectedYear()), per, (short)1);
                        verifikasiSession.updateStatusDataLraByPemda(sp, Short.valueOf(getSelectedYear()), per);
                    }
                    else if(sp.getIkd().trim().equals("LRA Pertanggungjawaban")) {
//                        verifikasiSession.updateStatusDataLraByPemda(sp, Short.valueOf(getSelectedYear()), (short)13, (short)1);
                        verifikasiSession.updateStatusDataLraByPemda(sp, Short.valueOf(getSelectedYear()), (short)13);
                    }
                    else if(sp.getIkd().trim().equals("Neraca Semester 1")) {
                        verifikasiSession.updateStatusDataNeracaByPemda(sp, Short.valueOf(getSelectedYear()), (short)1);
                    }
                    else if(sp.getIkd().trim().equals("Neraca Semester 2")) {
                        verifikasiSession.updateStatusDataNeracaByPemda(sp, Short.valueOf(getSelectedYear()), (short)2);
                    }
                    else if(sp.getIkd().trim().equals("Arus Kas")) {
                        verifikasiSession.updateStatusDataArusKasByPemda(sp, Short.valueOf(getSelectedYear()));
                    }
                    else if(sp.getIkd().trim().startsWith("Laporan Operasional ")) {
                        short per = Short.valueOf(sp.getIkd().trim().substring(sp.getIkd().trim().length()-1, sp.getIkd().trim().length()));
                        verifikasiSession.updateStatusDataLOByPemda(sp, Short.valueOf(getSelectedYear()), per);
                    }
                    else if(sp.getIkd().trim().equals("Laporan Perubahan Saldo")) {
                        verifikasiSession.updateStatusDataLPSalByPemda(sp, Short.valueOf(getSelectedYear()));
                    }
                    else if(sp.getIkd().trim().equals("Laporan Perubahan Ekuitas")) {
                        verifikasiSession.updateStatusDataLpeByPemda(sp, Short.valueOf(getSelectedYear()));
                    }
                    else if(sp.getIkd().trim().equals("Pinjaman Daerah")) {
                        verifikasiSession.updateStatusDataPinjamanDaerahByPemda(sp, Short.valueOf(getSelectedYear()));
                    }
                    else if(sp.getIkd().trim().equals("Perhitungan Fihak Ketiga")) {
                        verifikasiSession.updateStatusDataPFKByPemda(sp, Short.valueOf(getSelectedYear()));
                    }
                    else if(sp.getIkd().trim().startsWith("DTH Periode ")) {
                        short per = Short.valueOf(sp.getIkd().trim().substring(sp.getIkd().trim().length()-1, sp.getIkd().trim().length()));
                        verifikasiSession.updateStatusDataApbdByPemda(sp, Short.valueOf(getSelectedYear()), per);
                    }
                    
                    
                }
                System.out.println("sp " +sp.getStatusData().getNama());
                
            }
//            User user = (User) event.getObject();
//            System.out.println("Edit: " + user);
//
//            userBean.update(user.getUserId(), user.getUsername(), user.getPassword(), 
//                User.AccountType.valueOf(user.getAccountType()), user.getFirstname(), user.getLastname(),
//                user.getTeam() == null ? null : user.getTeam().getTeamId());
//            System.out.println("User " + user.getUserId() + " updated: " + user.getFirstname());
    } catch (Exception ex) {
        Logger.getLogger(UpdateStatusDataMBean.class.getName()).log(Level.SEVERE, null, ex);
    } 
}
    
}