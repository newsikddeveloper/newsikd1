package app.sikd.web.statusdata;

import app.sikd.aas.ServerUtil;
import app.sikd.aas.SessionUtil;
import app.sikd.entity.Pemda;
import app.sikd.entity.backoffice.StatusPemda;
import app.sikd.login.ejb.session.AdministrasiSessionBeanRemote;
import app.sikd.login.ejb.session.NewLoginSessionBeanRemote;
import app.sikd.office.ejb.newsession.NewVerifikasiSessionBeanRemote;
import app.sikd.util.SIKDUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.naming.Context;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author PC04
 */
@ManagedBean(name = "statusListDataPdMBean")
@ViewScoped

public class StatusListDataPdMBean implements Serializable {
    List<String> years;
    String selectedYear;
    String labelYear;
    List<Pemda> pemdas;

    AdministrasiSessionBeanRemote administrasiSession;
    NewLoginSessionBeanRemote loginSession;
    NewVerifikasiSessionBeanRemote verifikasiSession;
    long grupId;
    
    List<StatusPemda> statusPemdas;

    
    @PostConstruct
    public void init() {
        try {
            labelYear = "";
            String username = SessionUtil.getCookieUsername();
            FacesContext context = FacesContext.getCurrentInstance();
            ExternalContext excontext = context.getExternalContext();
            HttpServletRequest httpServletRequest = (HttpServletRequest) excontext.getRequest();
            String path = httpServletRequest.getRequestURI();
            String namamenu = SIKDUtil.getNamaMenu(path);
            if (username == null) {
                throw new Exception("Identitas pengguna tidak bisa ditemukan atau sudah lewat masa sesi koneksi. "
                        + "Pengguna harus kembali melakukan login.");
            }
            String grupids = SessionUtil.getCookieGroupid();
            if (grupids != null) {
                grupId = Long.valueOf(grupids);
            }

            Context ctxLogin = ServerUtil.getLoginContext();
            Context ctxOffice = ServerUtil.getOfficeContext();
            administrasiSession = (AdministrasiSessionBeanRemote) ctxLogin.lookup("AdministrasiSessionBean/remote");
            loginSession = (NewLoginSessionBeanRemote) ctxLogin.lookup("NewLoginSessionBean/remote");
            verifikasiSession = (NewVerifikasiSessionBeanRemote) ctxOffice.lookup("NewVerifikasiSessionBean/remote");

            years = new ArrayList();
            statusPemdas = new ArrayList();
            if (loginSession.canRead(grupId, namamenu)) {
                years = SIKDUtil.getYears((short) 2010);
                pemdas = administrasiSession.getPemdas();
            }
        } catch (Exception ex) {
            System.out.println("Error bro:" + ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Kegagalan",
                    "Gagal dalam mengambil data User Group. "
                    + ex.toString()));
        }

    }

    public String getSelectedYear() {
        return selectedYear;
    }

    public void setSelectedYear(String selectedYear) {
        this.selectedYear = selectedYear;
    }

    public void onYearChange() {
        labelYear = ""; 
        statusPemdas.clear();
        if (selectedYear != null && !selectedYear.trim().equals("")) {
            try {
                labelYear = "TAHUN ANGGARAN " + selectedYear;
                statusPemdas=verifikasiSession.getStatusDataPinjamanDaerahByPemda(pemdas, Short.valueOf(selectedYear));
            } catch (Exception ex) {
                Logger.getLogger(StatusListDataPdMBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public List<String> getYears() {
        return years;
    }

    public String getLabelYear() {
        return labelYear;
    }

    public void setLabelYear(String labelYear) {
        this.labelYear = labelYear;
    }

    public List<StatusPemda> getStatusPemdas() {
        return statusPemdas;
    }

    public void setStatusPemdas(List<StatusPemda> statusPemdas) {
        this.statusPemdas = statusPemdas;
    }
    
}