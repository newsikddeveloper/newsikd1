/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.web.laporan.apbd;

import java.io.Serializable;

/**
 *
 * @author sora
 */
public class LampiranVIAPBDObj extends DaftarPegawaiObj implements Serializable{
    int jumlah;

    public LampiranVIAPBDObj() {
    }

    public LampiranVIAPBDObj(String golongan, int jumlahEselon1, int jumlahEselon2, int jumlahEselon3, int jumlahEselon4, int jumlahEselon5, int jumlahFungsional, int jumlahStaf, int jumlah) {
        super(golongan, jumlahEselon1, jumlahEselon2, jumlahEselon3, jumlahEselon4, jumlahEselon5, jumlahFungsional, jumlahStaf);
        this.jumlah = jumlah;
    }

    public LampiranVIAPBDObj(String golongan, int jumlahEselon1, int jumlahEselon2, int jumlahEselon3, int jumlahEselon4, int jumlahEselon5, int jumlahFungsional, int jumlahStaf, int jumlah, String fontStyle) {
        super(golongan, jumlahEselon1, jumlahEselon2, jumlahEselon3, jumlahEselon4, jumlahEselon5, jumlahFungsional, jumlahStaf);
        this.jumlah = jumlah;
        setFontStyle(fontStyle);
    }

    public int getJumlah() {
        jumlah = getJumlahEselon1() + getJumlahEselon2() + getJumlahEselon3() + 
                getJumlahEselon4() + getJumlahEselon5() + getJumlahFungsional() + getJumlahStaf();
        return jumlah;
    }

    public void setJumlah(int jumlah) {
        this.jumlah = jumlah;
    }
    
    
    
    
    
}
