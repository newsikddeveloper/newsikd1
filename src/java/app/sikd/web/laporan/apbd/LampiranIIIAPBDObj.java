/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.web.laporan.apbd;

import java.io.Serializable;

/**
 *
 * @author sora
 */
public class LampiranIIIAPBDObj implements Serializable{
    private String kode;
    private String uraian;
    private String jumlah;
    private String dasarHukum;
    private String fontStyle;
    private String marginStyle;
    

    public LampiranIIIAPBDObj() {
    }

    public LampiranIIIAPBDObj(String kode, String uraian, String jumlah, String dasarHukum, String fontStyle, String marginStyle) {
        this.kode = kode;
        this.uraian = uraian;
        this.jumlah = jumlah;
        this.dasarHukum = dasarHukum;
        this.fontStyle = fontStyle;
        this.marginStyle = marginStyle;
    }

    public String getDasarHukum() {
        return dasarHukum;
    }

    public void setDasarHukum(String dasarHukum) {
        this.dasarHukum = dasarHukum;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getUraian() {
        return uraian;
    }

    public void setUraian(String uraian) {
        this.uraian = uraian;
    }

    public String getJumlah() {
        return jumlah;
    }

    public void setJumlah(String jumlah) {
        this.jumlah = jumlah;
    }

    public String getFontStyle() {
        return fontStyle;
    }

    public void setFontStyle(String fontStyle) {
        this.fontStyle = fontStyle;
    }

    public String getMarginStyle() {
        return marginStyle;
    }

    public void setMarginStyle(String marginStyle) {
        this.marginStyle = marginStyle;
    }
    
    
    
    
    
}
