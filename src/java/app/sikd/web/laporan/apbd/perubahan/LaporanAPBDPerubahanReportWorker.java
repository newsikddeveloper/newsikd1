/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.laporan.apbd.perubahan;

import app.sikd.aas.ServerUtil;
import app.sikd.entity.Pemda;
import app.sikd.entity.apbd.LampiranIPerdaAPBD;
import app.sikd.entity.backoffice.APBD;
import app.sikd.entity.backoffice.APBDPerda;
import app.sikd.office.ejb.session.APBDSessionBeanRemote;
import app.sikd.util.SIKDUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;

/**
 *
 * @author Nongky
 * @author detra
 */
public class LaporanAPBDPerubahanReportWorker {

    private APBDSessionBeanRemote service;
    private List<LampiranIAPBDPObj> apbds = new ArrayList<>();
    private APBD apbd;

    String nomor;
    String tanggal;

    public LaporanAPBDPerubahanReportWorker() {
        try {
            Context context = ServerUtil.getOfficeContext();
            service = (APBDSessionBeanRemote) context.lookup("APBDSessionBean/remote");
        } catch (Exception ex) {
            Logger.getLogger(LaporanAPBDPerubahanReportWorker.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<LampiranIAPBDPObj> updateLaporanPerubahanReport(short _year, Pemda _pemda, short _coaType) throws Exception {
        apbds.clear();
        initDataApbd(_year, _pemda.getKodeSatker(), _coaType);
        List<LampiranIAPBDPObj> objs = initDataPerubahan(_year, _pemda.getKodeSatker(), _coaType);
        if (apbds != null && objs != null) {
            for (LampiranIAPBDPObj apbd : apbds) {
                if (apbd.getNourut().trim().length() != 1) {
                    if (apbd.getUraian().trim() != null && !apbd.getUraian().trim().equals("")) {
                        int ii = 0;
                        boolean find = false;
                        while (ii < objs.size() && find == false) {
                            if (objs.get(ii).getUraian().trim().equals(apbd.getUraian().trim()) && objs.get(ii).getNourut().trim().equals(apbd.getNourut().trim())) {
                                apbd.setDperubahan(objs.get(ii).getDperubahan());
                                find = true;
                            }
                            ii++;
                        }
                    }
                }
            }
        }
        return apbds;
    }

    public final List<LampiranIAPBDPObj> initDataPerubahan(short _year, String _kodeSatker, short _coaType) {
        List<LampiranIAPBDPObj> objs = new ArrayList();
        try {
            APBDPerda per = service.getAPBDPerubahanPerdaNumber(_year, _kodeSatker, _coaType);
            if (per != null) {
                this.nomor = per.getNomor();
                this.tanggal = SIKDUtil.dateToString(per.getDate());
            }
            List<LampiranIPerdaAPBD> ls = service.getLampiranIPerdaAPBDPerubahan(_year, _kodeSatker, _coaType);
            String k = "";
            double j1 = 0, j2 = 0, j31 = 0, j32 = 0;
            if (ls != null && ls.size() > 0) {
                for (int i = 0; i < ls.size(); i++) {
                    LampiranIPerdaAPBD l = ls.get(i);
                    if (i == 0 && l.getKode().trim().length() == 1) {
                        k = l.getKode().trim();
                    } else {
                        if (l.getKode().trim().length() == 1 || l.getKode().trim().length() == 3) {
                            if (l.getKode().trim().length() == 1 && !k.trim().equals(l.getKode().trim())) {
                                if (k.trim().equals("4")) {
                                    objs.add(new LampiranIAPBDPObj(" ", "JUMLAH PENDAPATAN", 0, j1));
                                    objs.get(objs.size() - 1).setFontStyle("font-weight: bold;");
                                    objs.add(new LampiranIAPBDPObj(" ", " ", 0, 0));
                                    k = l.getKode().trim();
                                } else if (k.trim().equals("5")) {
                                    objs.add(new LampiranIAPBDPObj(" ", "JUMLAH BELANJA", 0, j2));
                                    objs.get(objs.size() - 1).setFontStyle("font-weight: bold;");
                                    objs.add(new LampiranIAPBDPObj(" ", " ", 0, 0));

                                    objs.add(new LampiranIAPBDPObj(" ", "SURPLUS/DEFISIT", 0, (j1 - j2)));
                                    objs.get(objs.size() - 1).setFontStyle("font-weight: bold;");
                                    objs.add(new LampiranIAPBDPObj(" ", " ", 0, 0));

                                    k = l.getKode().trim();
                                } else {
                                    k = l.getKode().trim();
                                }
                            } else if (l.getKode().trim().length() == 3 && !k.trim().equals(l.getKode().trim())) {
                                if (k.trim().equals("6")) {
                                    k = l.getKode().trim();
                                }

                                if (!l.getKode().trim().equals(k.trim())) {
                                    if (k.trim().equals("6.1")) {
                                        objs.add(new LampiranIAPBDPObj(" ", "Jumlah Penerimaan Pembiayaan", 0, (j31)));
                                        objs.get(objs.size() - 1).setFontStyle("font-weight: bold;");
                                        objs.add(new LampiranIAPBDPObj(" ", " ", 0, 0));
                                        k = l.getKode().trim();
                                    }
                                }
                            }
                        } else {
                            if (k.trim().equals("4")) {
                                j1 += l.getJumlah();
                            } else if (k.trim().equals("5")) {
                                j2 += l.getJumlah();
                            } else if (k.trim().equals("6.1")) {
                                j31 += l.getJumlah();
                            } else if (k.trim().equals("6.2")) {
                                j32 += l.getJumlah();
                            }
                        }
                    }

                    objs.add(new LampiranIAPBDPObj(l.getKode(), l.getUraian(), 0, l.getJumlah()));
                    if (l.getKode() != null && !l.getKode().trim().equals("")) {
                        if (l.getKode().trim().length() == 1 || l.getKode().trim().length() == 3) {
                            objs.get(objs.size() - 1).setFontStyle("font-weight: bold;");
                        }
                    }
                }
                if (k.trim().equals("5")) {
                    objs.add(new LampiranIAPBDPObj(" ", "JUMLAH BELANJA", 0, j2));
                    objs.get(objs.size() - 1).setFontStyle("font-weight: bold;");
                    objs.add(new LampiranIAPBDPObj(" ", " ", 0, 0));

                    objs.add(new LampiranIAPBDPObj(" ", "SURPLUS/DEFISIT", 0, (j1 - j2)));
                    objs.get(objs.size() - 1).setFontStyle("font-weight: bold;");
                    objs.add(new LampiranIAPBDPObj(" ", " ", 0, 0));
                } else if (k.trim().equals("6.1")) {
                    objs.add(new LampiranIAPBDPObj(" ", "Jumlah Penerimaan Pembiayaan", 0, j31));
                    objs.get(objs.size() - 1).setFontStyle("font-weight: bold;");
                    objs.add(new LampiranIAPBDPObj(" ", " ", 0, 0));
                } else if (k.trim().equals("6.2")) {
                    objs.add(new LampiranIAPBDPObj(" ", "Jumlah Pengeluaran Pembiayaan", 0, j32));
                    objs.get(objs.size() - 1).setFontStyle("font-weight: bold;");
                    objs.add(new LampiranIAPBDPObj(" ", " ", 0, 0));
                }

                objs.add(new LampiranIAPBDPObj(" ", "PEMBIAYAAN NETTO", 0, (j31 - j32)));
                objs.get(objs.size() - 1).setFontStyle("font-weight: bold;");
                objs.add(new LampiranIAPBDPObj(" ", " ", 0, 0));

                objs.add(new LampiranIAPBDPObj(" ", "Sisa Lebih Pembiayaan Anggaran Tahun Berkenaan (SILPA)", 0, ((j1 - j2) + (j31 - j32))));
                objs.get(objs.size() - 1).setFontStyle("font-weight: bold;");
                objs.add(new LampiranIAPBDPObj(" ", " ", 0, 0));

            }

        } catch (Exception ex) {
            Logger.getLogger(LaporanAPBDPerubahanReportWorker.class.getName()).log(Level.SEVERE, null, ex);
        }
        return objs;
    }

    public final void initDataApbd(short _year, String _kodeSatker, short _coaType) {
        try {
            System.out.println(_year+" "+_kodeSatker+" "+_coaType);
            List<LampiranIPerdaAPBD> ls = service.getLampiranIPerdaAPBDMurni(_year, _kodeSatker, _coaType);

            String k = "";
            double j1 = 0, j2 = 0, j31 = 0, j32 = 0;
            if (ls != null && ls.size() > 0) {
                for (int i = 0; i < ls.size(); i++) {
                    LampiranIPerdaAPBD l = ls.get(i);
                    if (i == 0 && l.getKode().trim().length() == 1) {
                        k = l.getKode().trim();
                    } else {
                        if (l.getKode().trim().length() == 1 || l.getKode().trim().length() == 3) {
                            if (l.getKode().trim().length() == 1 && !k.trim().equals(l.getKode().trim())) {
                                if (k.trim().equals("4")) {
                                    apbds.add(new LampiranIAPBDPObj(" ", "JUMLAH PENDAPATAN", j1, 0));
                                    apbds.get(apbds.size() - 1).setFontStyle("font-weight: bold;");
                                    apbds.add(new LampiranIAPBDPObj(" ", " ", 0, 0));
                                    k = l.getKode().trim();
                                } else if (k.trim().equals("5")) {
                                    apbds.add(new LampiranIAPBDPObj(" ", "JUMLAH BELANJA", j2, 0));
                                    apbds.get(apbds.size() - 1).setFontStyle("font-weight: bold;");
                                    apbds.add(new LampiranIAPBDPObj(" ", " ", 0, 0));

                                    apbds.add(new LampiranIAPBDPObj(" ", "SURPLUS/DEFISIT", (j1 - j2), 0));
                                    apbds.get(apbds.size() - 1).setFontStyle("font-weight: bold;");
                                    apbds.add(new LampiranIAPBDPObj(" ", " ", 0, 0));

                                    k = l.getKode().trim();
                                } else {
                                    k = l.getKode().trim();
                                }
                            } else if (l.getKode().trim().length() == 3 && !k.trim().equals(l.getKode().trim())) {
                                if (k.trim().equals("6")) {
                                    k = l.getKode().trim();
                                }

                                if (!l.getKode().trim().equals(k.trim())) {
                                    if (k.trim().equals("6.1")) {
                                        apbds.add(new LampiranIAPBDPObj(" ", "Jumlah Penerimaan Pembiayaan", j31, 0));
                                        apbds.get(apbds.size() - 1).setFontStyle("font-weight: bold;");
                                        apbds.add(new LampiranIAPBDPObj(" ", " ", 0, 0));
                                        k = l.getKode().trim();
                                    }
                                }
                            }
                        } else {
                            if (k.trim().equals("4")) {
                                j1 += l.getJumlah();
                            } else if (k.trim().equals("5")) {
                                j2 += l.getJumlah();
                            } else if (k.trim().equals("6.1")) {
                                j31 += l.getJumlah();
                            } else if (k.trim().equals("6.2")) {
                                j32 += l.getJumlah();
                            }
                        }
                    }

                    apbds.add(new LampiranIAPBDPObj(l.getKode(), l.getUraian(), l.getJumlah(), 0));
                    if (l.getKode() != null && !l.getKode().trim().equals("")) {
                        if (l.getKode().trim().length() == 1 || l.getKode().trim().length() == 3) {
                            apbds.get(apbds.size() - 1).setFontStyle("font-weight: bold;");
                        }
                    }
                }
                if (k.trim().equals("6.1")) {
                    apbds.add(new LampiranIAPBDPObj(" ", "Jumlah Penerimaan Pembiayaan", j31, 0));
                    apbds.get(apbds.size() - 1).setFontStyle("font-weight: bold;");
                    apbds.add(new LampiranIAPBDPObj(" ", " ", 0, 0));
                } else if (k.trim().equals("6.2")) {
                    apbds.add(new LampiranIAPBDPObj(" ", "Jumlah Pengeluaran Pembiayaan", j32, 0));
                    apbds.get(apbds.size() - 1).setFontStyle("font-weight: bold;");
                    apbds.add(new LampiranIAPBDPObj(" ", " ", 0, 0));
                }

                apbds.add(new LampiranIAPBDPObj(" ", "PEMBIAYAAN NETTO", (j31 - j32), 0));
                apbds.get(apbds.size() - 1).setFontStyle("font-weight: bold;");
                apbds.add(new LampiranIAPBDPObj(" ", " ", 0, 0));

                apbds.add(new LampiranIAPBDPObj(" ", "Sisa Lebih Pembiayaan Anggaran Tahun Berkenaan (SILPA)", ((j1 - j2) + (j31 - j32)), 0));
                apbds.get(apbds.size() - 1).setFontStyle("font-weight: bold;");
                apbds.add(new LampiranIAPBDPObj(" ", " ", 0, 0));

            }
        } catch (Exception ex) {
            Logger.getLogger(LaporanAPBDPerubahanReportWorker.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
