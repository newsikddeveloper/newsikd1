/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.laporan.apbd.perubahan;

import app.sikd.util.SIKDUtil;
import app.sikd.web.common.bean.ABasicFilteredBackBean;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Nongky
 */
@ManagedBean(name = "laporanAPBDPerubahan")
@ViewScoped
public class LaporanAPBDPerubahanBackBean extends ABasicFilteredBackBean {

    private LaporanAPBDPerubahanReportWorker reportWorker;

    @PostConstruct
    @Override
    protected void init() {
        super.init();
        reportWorker = new LaporanAPBDPerubahanReportWorker();
        setPage("/Laporan/LaporanAPBD/perubahan/perubahan-content.xhtml");
    }

    private List<LampiranIAPBDPObj> reports;

    @Override
    public void onChangeProcess() {
        try {
            reports = reportWorker.updateLaporanPerubahanReport(new Short(year), pemda, SIKDUtil.COA_PMDN13);
        } catch (Exception ex) {
            Logger.getLogger(LaporanAPBDPerubahanBackBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<LampiranIAPBDPObj> getData() {
        return reports;
    }
}
