/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.laporan.apbd;

import app.sikd.util.SIKDUtil;
import app.sikd.web.common.bean.ABasicFilteredBackBean;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Nongky
 */
@ManagedBean(name = "apbdLampiranIV")
@ViewScoped
public class LampiranIVBackBean extends ABasicFilteredBackBean {

    private LampiranIVReportWorker reportWorker;

    @PostConstruct
    @Override
    protected void init() {
        super.init();
        reportWorker = new LampiranIVReportWorker();
    }

    private List<LampiranIVAPBDObj> reports;

    @Override
    public void onChangeProcess() {
        try {
            reports = reportWorker.updateLampiranIVReport(new Short(year), pemda, SIKDUtil.COA_PMDN13);
        } catch (Exception ex) {
            Logger.getLogger(LampiranIVBackBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<LampiranIVAPBDObj> getData() {
        return reports;
    }
}
