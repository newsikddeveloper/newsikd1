/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.laporan.apbd;

import app.sikd.aas.ServerUtil;
import app.sikd.entity.Pemda;
import app.sikd.entity.apbd.LampiranIVPerdaAPBD;
import app.sikd.entity.backoffice.APBD;
import app.sikd.entity.backoffice.APBDPerda;
import app.sikd.office.ejb.session.APBDSessionBeanRemote;
import app.sikd.util.SIKDUtil;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;

/**
 *
 * @author Nongky
 */
public class LampiranIVReportWorker {

    private APBDSessionBeanRemote service;
    private List<LampiranIVAPBDObj> apbds = new ArrayList<>();
    private APBD apbd;
    private String nomor, tanggal;

    public LampiranIVReportWorker() {
        try {
            Context context = ServerUtil.getOfficeContext();
            service = (APBDSessionBeanRemote) context.lookup("APBDSessionBean/remote");
        } catch (Exception ex) {
            Logger.getLogger(LampiranIVReportWorker.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<LampiranIVAPBDObj> updateLampiranIVReport(short _year, Pemda _pemda, short _coaType) throws Exception {
        apbds.clear();
        this.nomor = "";
        this.tanggal = "";
        APBDPerda per = service.getAPBDMurniPerdaNumber(_year, _pemda.getKodeSatker(), _coaType);
        if (per != null) {
            this.nomor = per.getNomor();
            this.tanggal = SIKDUtil.dateToString(per.getDate());
        }
        List<LampiranIVPerdaAPBD> ls = service.getLampiranIVPerdaAPBDMurni(_year, _pemda.getKodeSatker(), _coaType);

        if (ls != null && ls.size() > 0) {
            for (LampiranIVPerdaAPBD l : ls) {
                String fontStyle = "";
                if (l.getKode() != null && !l.getKode().trim().equals("") && l.getKode().trim().length() < 10) {
                    fontStyle = "font-weight: bold;";
                }
                apbds.add(new LampiranIVAPBDObj(l.getKode(), l.getUrusanPemerintahan(), getStringValue(l.getPegawai()), getStringValue(l.getBarangJasa()), getStringValue(l.getModal()), getStringValue(l.getPegawai() + l.getBarangJasa() + l.getModal()), fontStyle));
            }
        }
        return apbds;
    }

    private String getStringValue(double jj) {
        Locale.setDefault(Locale.GERMAN);
        DecimalFormat df = new DecimalFormat("#,###,##0.00");
        df.setNegativePrefix("( ");
        df.setNegativeSuffix(" )");
        String result = df.format(jj);
        return result;
    }

}
