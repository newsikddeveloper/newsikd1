/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.laporan.apbd;

import app.sikd.aas.ServerUtil;
import app.sikd.entity.Pemda;
import app.sikd.entity.apbd.LampiranVPerdaAPBD;
import app.sikd.entity.backoffice.APBD;
import app.sikd.entity.backoffice.APBDPerda;
import app.sikd.office.ejb.session.APBDSessionBeanRemote;
import app.sikd.util.SIKDUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;

/**
 *
 * @author Nongky
 */
public class LampiranVReportWorker {

    private APBDSessionBeanRemote service;
    private List<LampiranVAPBDObj> apbds = new ArrayList<>();
    private APBD apbd;
    private String nomor, tanggal;

    public LampiranVReportWorker() {
        try {
            Context context = ServerUtil.getOfficeContext();
            service = (APBDSessionBeanRemote) context.lookup("APBDSessionBean/remote");
        } catch (Exception ex) {
            Logger.getLogger(LampiranVReportWorker.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<LampiranVAPBDObj> updateLampiranVReport(short _year, Pemda _pemda, short _coaType) throws Exception {
        apbds.clear();
        this.nomor = "";
        this.tanggal = "";
        APBDPerda per = service.getAPBDMurniPerdaNumber(_year, _pemda.getKodeSatker(), _coaType);
        if (per != null) {
            this.nomor = per.getNomor();
            this.tanggal = SIKDUtil.dateToString(per.getDate());
        }
        List<LampiranVPerdaAPBD> ls = service.getLampiranVPerdaAPBDMurni(_year, _pemda.getKodeSatker(), _coaType);
        if (ls != null && ls.size() > 0) {
            for (LampiranVPerdaAPBD l : ls) {
                apbds.add(new LampiranVAPBDObj(l.getKode(), l.getUrusanPemerintahan(), l.getPegawaiTidakLangsung(), l.getBelanjaLainnya() - (l.getPegawaiTidakLangsung() + l.getPegawai() + l.getBarangJasa() + l.getModal()), l.getPegawai(), l.getBarangJasa(), l.getModal(), l.getBelanjaLainnya()));
            }
        }
        return apbds;
    }

}
