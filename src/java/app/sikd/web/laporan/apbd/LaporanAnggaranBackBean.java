/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.laporan.apbd;

import app.sikd.web.common.ASinglePageBackbean;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Nongky
 */
@ManagedBean(name = "laporanAnggaranBackBean")
@ViewScoped
public class LaporanAnggaranBackBean extends ASinglePageBackbean {

    @PostConstruct
    private void init() {
        setPage("/Laporan/LaporanAPBD/apbd/index.xhtml");
    }
}
