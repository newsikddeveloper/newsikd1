/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.laporan.apbd;

import app.sikd.entity.apbd.LampiranIIPerdaAPBD;
import app.sikd.util.SIKDUtil;
import app.sikd.web.common.bean.ABasicFilteredBackBean;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Nongky
 */
@ManagedBean(name = "apbdLampiranII")
@ViewScoped
public class LampiranIIBackBean extends ABasicFilteredBackBean {

    private LampiranIIReportWorker reportWorker;

    @PostConstruct
    @Override
    protected void init() {
        super.init();
        reportWorker = new LampiranIIReportWorker();
    }

    private List<LampiranIIPerdaAPBD> pendapatanReports;
    private List<LampiranIIPerdaAPBD> pembiayaanReports;

    @Override
    public void onChangeProcess() {
        try {
            reportWorker.updateLampiranIIReport(new Short(year), pemda, SIKDUtil.COA_PMDN13);
            pendapatanReports = reportWorker.getApbds45();
            pembiayaanReports = reportWorker.getApbds6();
        } catch (Exception ex) {
            Logger.getLogger(LampiranIIBackBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<LampiranIIPerdaAPBD> getPendapatanData() {
        return pendapatanReports;
    }

    public List<LampiranIIPerdaAPBD> getPembiayaanData() {
        return pembiayaanReports;
    }

    public List getData() {
        return null;
    }

}
