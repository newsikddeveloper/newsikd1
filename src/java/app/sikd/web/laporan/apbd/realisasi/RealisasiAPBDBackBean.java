/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.laporan.apbd.realisasi;

import app.sikd.entity.apbd.LampiranILra;
import app.sikd.web.common.bean.ACOAFilteredBackBean;
import app.sikd.web.common.bean.IPeriodFilteredBean;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Nongky
 */
@ManagedBean(name = "realisasiAPBD")
@ViewScoped
public class RealisasiAPBDBackBean extends ACOAFilteredBackBean implements IPeriodFilteredBean {

    private String periode;
    private RealisasiAPBDReportWorker reportWorker;

    @PostConstruct
    @Override
    protected void init() {
        super.init();
        reportWorker = new RealisasiAPBDReportWorker();
    }

    private List<LampiranILra> reportData;

    public List<LampiranILra> getData() {
        return reportData;
    }

    @Override
    protected void onChangeProcess() {
        if (periode != null && periode.length() > 0) {
            try {
                reportData = reportWorker.updateRealisasiAPBDReport(new Short(year), pemda.getKodeSatker(), new Short(periode), new Short(coaType));
            } catch (Exception ex) {
                Logger.getLogger(RealisasiAPBDBackBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public void setPeriode(String _periode) {
        periode = _periode;
    }

    @Override
    public String getPeriode() {
        return periode;
    }

}
