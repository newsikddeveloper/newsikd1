/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.laporan.apbd.realisasi;

import app.sikd.util.SIKDUtil;
import app.sikd.web.common.bean.ABasicFilteredBackBean;
import app.sikd.web.common.bean.IFungsiFilteredBean;
import app.sikd.web.common.bean.IPeriodFilteredBean;
import app.sikd.web.fungsi.NilaiFungsiPemdaObj;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Nongky
 */
@ManagedBean(name = "realisasiFungsiBackBean")
@ViewScoped
public class RealisasiFungsiBackBean extends ABasicFilteredBackBean implements IPeriodFilteredBean, IFungsiFilteredBean {

    private String periode;
    private String fungsi;

    private RealisasiFungsiReportWorker reportWorker;

    @PostConstruct
    @Override
    protected void init() {
        super.init();
        reportWorker = new RealisasiFungsiReportWorker();
    }

    @Override
    protected void onChangeProcess() {
        if (periode != null && periode.length() > 0 && fungsi != null && fungsi.length() > 0) {
            try {
                reportData = reportWorker.updateRealisasiFungsiReport(new Short(year), pemda.getKodeSatker(), new Short(periode), SIKDUtil.APBD_MURNI_SHORT, fungsi);
            } catch (Exception ex) {
                Logger.getLogger(RealisasiFungsiBackBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private List<NilaiFungsiPemdaObj> reportData;

    public List<NilaiFungsiPemdaObj> getData() {
        return reportData;
    }

    @Override
    public void setPeriode(String _periode) {
        periode = _periode;
    }

    @Override
    public String getPeriode() {
        return periode;
    }

    @Override
    public String getFungsi() {
        return fungsi;
    }

    @Override
    public void setFungsi(String _fungsi) {
        fungsi = _fungsi;
    }

}
