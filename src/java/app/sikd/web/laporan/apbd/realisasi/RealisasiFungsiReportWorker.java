/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.laporan.apbd.realisasi;

import app.sikd.aas.ServerUtil;
import app.sikd.entity.backoffice.NilaiFungsiPemda;
import app.sikd.office.ejb.session.APBDSessionBeanRemote;
import app.sikd.util.SIKDUtil;
import app.sikd.web.fungsi.NilaiFungsiPemdaObj;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;

/**
 *
 * @author Nongky
 */
public class RealisasiFungsiReportWorker {

    private APBDSessionBeanRemote service;

    public RealisasiFungsiReportWorker() {
        try {
            Context context = ServerUtil.getOfficeContext();
            service = (APBDSessionBeanRemote) context.lookup("APBDSessionBean/remote");
        } catch (Exception ex) {
            Logger.getLogger(RealisasiFungsiReportWorker.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private List<NilaiFungsiPemdaObj> objects = new ArrayList<>();

    public List<NilaiFungsiPemdaObj> updateRealisasiFungsiReport(short _year, String _kodeSatker, short _month, short _kodeData, String _kodeFungsi) throws Exception {
        List<NilaiFungsiPemda> nfp = service.getRealisasiKegiatanBerdasarkanFungsi(_year, _month, _kodeSatker, _kodeData, _kodeFungsi);
        objects.clear();
        if (nfp != null && nfp.size() > 0) {
            for (int i = 0; i < nfp.size(); i++) {
                objects.add(new NilaiFungsiPemdaObj(String.valueOf(i + 1), nfp.get(i).getKodePemda(), nfp.get(i).getNamaPemda(), nfp.get(i).getNilai()));
            }
        }
        return objects;
    }

}
