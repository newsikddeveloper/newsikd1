/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.laporan.apbd.realisasi;

import app.sikd.web.common.bean.ABasicFilteredBackBean;
import app.sikd.web.common.bean.IPeriodFilteredBean;
import app.sikd.web.common.bean.ISumberDanaFilteredBean;
import app.sikd.web.fungsi.NilaiFungsiPemdaObj;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Nongky
 */
@ManagedBean(name = "realisasiSumberDanaBackBean")
@ViewScoped
public class RealisasiSumberDanaBackBean extends ABasicFilteredBackBean implements IPeriodFilteredBean, ISumberDanaFilteredBean {

    private String periode;
    private String sumberDana;

    private RealisasiSumberDanaReportWorker reportWorker;

    @PostConstruct
    @Override
    protected void init() {
        super.init();
        reportWorker = new RealisasiSumberDanaReportWorker();
    }

    @Override
    protected void onChangeProcess() {
        if (periode != null && periode.length() > 0 && sumberDana != null && sumberDana.length() > 0) {
            try {
                reportData = reportWorker.updateRealisasiSumberDanaReport(new Short(year),pemda.getKodeSatker(),new Short(periode), new Short(sumberDana));
            } catch (Exception ex) {
                Logger.getLogger(RealisasiSumberDanaBackBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private List<NilaiFungsiPemdaObj> reportData;
    public List<NilaiFungsiPemdaObj> getData() {
        return reportData;
    }

    @Override
    public void setPeriode(String _periode) {
        periode = _periode;
    }

    @Override
    public String getPeriode() {
        return periode;
    }

    @Override
    public String getSumberDana() {
        return sumberDana;
    }

    @Override
    public void setSumberDana(String _sumberDana) {
        sumberDana = _sumberDana;
    }

}
