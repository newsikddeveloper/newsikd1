/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.laporan.apbd.realisasi;

import app.sikd.aas.ServerUtil;
import app.sikd.entity.apbd.LampiranILra;
import app.sikd.office.ejb.session.APBDSessionBeanRemote;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;

/**
 *
 * @author Nongky
 */
public class RealisasiAPBDReportWorker {

    private APBDSessionBeanRemote service;

    public RealisasiAPBDReportWorker() {
        try {
            Context context = ServerUtil.getOfficeContext();
            service = (APBDSessionBeanRemote) context.lookup("APBDSessionBean/remote");
        } catch (Exception ex) {
            Logger.getLogger(RealisasiAPBDReportWorker.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<LampiranILra> updateRealisasiAPBDReport(short _year, String _kodeSatker, short _month, short _coaType) throws Exception {
        return service.getRealisasiAPBD(_year, _kodeSatker, _month, _coaType);
    }

}
