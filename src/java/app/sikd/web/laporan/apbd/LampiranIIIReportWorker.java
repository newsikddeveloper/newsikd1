/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.laporan.apbd;

import app.sikd.aas.ServerUtil;
import app.sikd.entity.Pemda;
import app.sikd.entity.apbd.LampiranIIIPerdaAPBD;
import app.sikd.entity.apbd.LampiranIPerdaAPBD;
import app.sikd.entity.backoffice.APBD;
import app.sikd.entity.backoffice.APBDPerda;
import app.sikd.entity.mgr.UserAccount;
import app.sikd.login.ejb.session.NewLoginSessionBeanRemote;
import app.sikd.office.ejb.session.APBDSessionBeanRemote;
import app.sikd.util.SIKDUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.naming.Context;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Nongky
 */
public class LampiranIIIReportWorker {

    private APBDSessionBeanRemote service;
    private List<LampiranIIIAPBDObj> apbds = new ArrayList<>();
    private APBD apbd;
    private String nomor, tanggal;

    public LampiranIIIReportWorker() {
        try {
            Context context = ServerUtil.getOfficeContext();
            service = (APBDSessionBeanRemote) context.lookup("APBDSessionBean/remote");
        } catch (Exception ex) {
            Logger.getLogger(LampiranIIIReportWorker.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<LampiranIIIAPBDObj> updateLampiranIIIReport(short _year, Pemda _pemda, String _skpd, short _coaType) throws Exception {
        apbds.clear();
        this.nomor = "";
        this.tanggal = "";
        String kodeSKPD = "";
        String urusan = "";
        if (_skpd != null && !_skpd.trim().equals("") && !_skpd.trim().equals("0")) {
            java.util.StringTokenizer tok = new java.util.StringTokenizer(_skpd, " ");
            if (tok.countTokens() > 0) {
                kodeSKPD = tok.nextToken();
            }
        }

        if (kodeSKPD != null && !kodeSKPD.trim().equals("") && kodeSKPD.trim().length() > 5) {
            urusan = kodeSKPD.trim().substring(0, 4);
            urusan = service.getUrusan(urusan);
        }
        APBDPerda per = service.getAPBDMurniPerdaNumber(_year, _pemda.getKodeSatker(), _coaType);
        if (per != null) {
            this.nomor = per.getNomor();
            this.tanggal = SIKDUtil.dateToString(per.getDate());
        }
        List<LampiranIIIPerdaAPBD> ls = service.getLampiranIIIPerdaAPBDMurni(_year, _pemda.getKodeSatker(), kodeSKPD, _coaType);

        if (ls != null && ls.size() > 0) {
            for (LampiranIIIPerdaAPBD l : ls) {
                if (l.getKode() == null || l.getKode().trim().equals("")) {
                    apbds.add(new LampiranIIIAPBDObj(l.getKode(), l.getUraian(), l.getJumlahString(), l.getDasarHukum(), "font-weight: bold;", ""));
                } else if (l.getKode().trim().length() < 25) {
                    apbds.add(new LampiranIIIAPBDObj(l.getKode(), l.getUraian(), l.getJumlahString(), l.getDasarHukum(), "font-weight: bold;", ""));
                } else {
                    apbds.add(new LampiranIIIAPBDObj(l.getKode(), l.getUraian(), l.getJumlahString(), l.getDasarHukum(), "", ""));
                }
            }
        }

        return apbds;
    }

}
