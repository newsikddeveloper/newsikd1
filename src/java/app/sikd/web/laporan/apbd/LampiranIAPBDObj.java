/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.laporan.apbd;

import java.io.Serializable;

/**
 *
 * @author Detra
 */
public class LampiranIAPBDObj implements Serializable{
    String nourut;
    String uraian;
    String jumlah;
    
    String fontStyle;
    private String marginStyle;

    public LampiranIAPBDObj() { 
    }

    public LampiranIAPBDObj(String nourut, String uraian, String jumlah) {
//        if( nourut!=null && !nourut.trim().equals("")){
//            if( nourut.trim().startsWith("5") ) this.nourut = "2"+nourut.substring(1).trim();
//            else if( nourut.trim().startsWith("4") ) this.nourut= "1"+nourut.substring(1).trim();        
//            else if( nourut.trim().startsWith("6") ) this.nourut = "3"+nourut.substring(1).trim();        
//            else this.nourut = nourut;
//        }
//        else 
        this.nourut = nourut;
        this.uraian = uraian;        
        this.jumlah = jumlah;
    }
    
    

    public String getJumlah() {
        return jumlah;
    }

    public void setJumlah(String jumlah) {
        this.jumlah = jumlah;
    }

    public String getNourut() {
          if( nourut!=null && !nourut.trim().equals("")){
            if( nourut.trim().startsWith("5") ) this.nourut = "2"+nourut.substring(1).trim();
            else if( nourut.trim().startsWith("4") ) this.nourut= "1"+nourut.substring(1).trim();       
            else if( nourut.trim().startsWith("6") ) this.nourut = "3"+nourut.substring(1).trim();           
            else if( nourut.trim().startsWith("7") ) this.nourut = "4"+nourut.substring(1).trim();           
        }
        return nourut;
    }

    public void setNourut(String nourut) {
//        if( nourut!=null && !nourut.trim().equals("")){
//            if( nourut.trim().startsWith("5") ) this.nourut = "2"+nourut.substring(1).trim();
//            else if( nourut.trim().startsWith("4") ) this.nourut= "1"+nourut.substring(1).trim();        
//            else if( nourut.trim().startsWith("6") ) this.nourut = "3"+nourut.substring(1).trim();        
//            else this.nourut = nourut;
//        }
//        else this.nourut = nourut;
        this.nourut = nourut;
    }

    public String getUraian() {
        return uraian;
    }

    public void setUraian(String uraian) {
        this.uraian = uraian;
    }

    public String getFontStyle() {
        if(fontStyle==null || fontStyle.trim().equals("")) fontStyle="font-weight: normal;";
        return fontStyle;
    }

    public void setFontStyle(String fontStyle) {
        this.fontStyle = fontStyle;
    }

    public String getMarginStyle() {
        return marginStyle;
    }

    public void setMarginStyle(String marginStyle) {
        this.marginStyle = marginStyle;
    }
    
}