/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.laporan.apbd;

import app.sikd.aas.ServerUtil;
import app.sikd.entity.Pemda;
import app.sikd.entity.apbd.LampiranIIPerdaAPBD;
import app.sikd.entity.backoffice.APBD;
import app.sikd.entity.backoffice.APBDPerda;
import app.sikd.office.ejb.session.APBDSessionBeanRemote;
import app.sikd.util.SIKDUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;

/**
 *
 * @author Nongky
 */
public class LampiranIIReportWorker {

    private APBDSessionBeanRemote service;
    private List<LampiranIIPerdaAPBD> apbds45 = new ArrayList<>();
    private List<LampiranIIPerdaAPBD> apbds6 = new ArrayList<>();
    private List<LampiranIIPerdaAPBD> apbds = new ArrayList<>();
    private APBD apbd;
    private String nomor, tanggal;

    public LampiranIIReportWorker() {
        try {
            Context context = ServerUtil.getOfficeContext();
            service = (APBDSessionBeanRemote) context.lookup("APBDSessionBean/remote");
        } catch (Exception ex) {
            Logger.getLogger(LampiranIIReportWorker.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void updateLampiranIIReport(short _year, Pemda _pemda, short _coaType) throws Exception {
        apbds.clear();
        apbds45.clear();
        apbds6.clear();
        this.nomor = "";
        this.tanggal = "";
        String kodeSKPD = "";
        String urusan = "";
        APBDPerda per = service.getAPBDMurniPerdaNumber(_year, _pemda.getKodeSatker(), _coaType);
        if (per != null) {
            this.nomor = per.getNomor();
            this.tanggal = SIKDUtil.dateToString(per.getDate());
        }
        this.apbds = service.getLampiranIIPerdaAPBDMurni(_year, _pemda.getKodeSatker(), _coaType);
        if (apbds != null) {
            for (LampiranIIPerdaAPBD apbd : apbds) {
                if (apbd.getKode().trim().length() > 1 && (apbd.getPembiayaanNeto() > 0 || apbd.getPenerimaan() > 0 || apbd.getSilpa() > 0)) {
                    apbds6.add(apbd);
                }
                apbds45.add(apbd);
            }
        }
    }

    public List<LampiranIIPerdaAPBD> getApbds45() {
        return apbds45;
    }

    public List<LampiranIIPerdaAPBD> getApbds6() {
        return apbds6;
    }

}
