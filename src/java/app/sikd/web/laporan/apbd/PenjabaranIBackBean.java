/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.laporan.apbd;

import app.sikd.util.SIKDUtil;
import app.sikd.web.common.bean.ACOAFilteredBackBean;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Nongky
 */
@ManagedBean(name = "penjabaranI")
@ViewScoped
public class PenjabaranIBackBean extends ACOAFilteredBackBean {

    private PenjabaranIReportWorker reportWorker;

    @PostConstruct
    @Override
    protected void init() {
        super.init();
        
        reportWorker = new PenjabaranIReportWorker();
    }

    private List<LampiranIAPBDObj> reports;

    @Override
    protected void onChangeProcess()  {
        try {
            reports = reportWorker.updatePenjabaranLampiranIReport(new Short(year), pemda,SIKDUtil.APBD_MURNI_SHORT,new Short(coaType));
        } catch (Exception ex) {
            Logger.getLogger(PenjabaranIBackBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<LampiranIAPBDObj> getData() {
        return reports;
    }

}
