/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.web.laporan.apbd;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Locale;

/**
 *
 * @author sora
 */
public class LampiranVAPBDObj implements Serializable{
    private String kode;
    private String urusan;
    private double pegawaiTidakLangsung;
    private double lainnya;
    private double pegawai;
    private double barangjasa;
    private double modal;
    private double total;

    public LampiranVAPBDObj() {
    }

    public LampiranVAPBDObj(String kode, String urusan, double pegawaiTidakLangsung, double lainnya, double pegawai, double barangjasa, double modal, double total) {
        this.kode = kode;
        this.urusan = urusan;
        this.pegawaiTidakLangsung = pegawaiTidakLangsung;
        this.lainnya = lainnya;
        this.pegawai = pegawai;
        this.barangjasa = barangjasa;
        this.modal = modal;
        this.total = total;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }
    
    public String getTotalAsString() {
        return getStringValue(total);
    }

    public void setTotalAsString(String totalAsString) {
        
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getUrusan() {
        return urusan;
    }

    public void setUrusan(String urusan) {
        this.urusan = urusan;
    }

    public double getPegawaiTidakLangsung() {
        return pegawaiTidakLangsung;
    }

    public void setPegawaiTidakLangsung(double pegawaiTidakLangsung) {
        this.pegawaiTidakLangsung = pegawaiTidakLangsung;
    }
    
    public String getPegawaiTidakLangsungAsString() {
        return getStringValue(pegawaiTidakLangsung);
    }

    public void setPegawaiTidakLangsungAsString(String pegawaiTidakLangsungAsString) {
        
    }

    public double getLainnya() {
        return lainnya;
    }

    public void setLainnya(double lainnya) {
        this.lainnya = lainnya;
    }
    
    public String getLainnyaAsString() {
        return getStringValue(lainnya);
    }

    public void setLainnyaAsString(String lainnyaAsString) {
        
    }

    public double getPegawai() {
        return pegawai;
    }

    public void setPegawai(double pegawai) {
        this.pegawai = pegawai;
    }
    
    public String getPegawaiAsString() {
        return getStringValue(pegawai);
    }

    public void setPegawaiAsString(String pegawaiAsString) {
        
    }

    public double getBarangjasa() {
        return barangjasa;
    }

    public void setBarangjasa(double barangjasa) {
        this.barangjasa = barangjasa;
    }
    
    public String getBarangjasaAsString() {
        return getStringValue(barangjasa);
    }

    public void setBarangjasaAsString(String barangjasaAsString) {
        
    }

    public double getModal() {
        return modal;
    }

    public void setModal(double modal) {
        this.modal = modal;
    }
    public void setModalAsString(String modalAsString){
        
    }
    public String getModalAsString(){
        return getStringValue(modal);
    }
    
    String getStringValue(double jj){
        Locale.setDefault(Locale.GERMAN);
        DecimalFormat df = new DecimalFormat("#,###,##0.00");
        df.setNegativePrefix("( "); 
        df.setNegativeSuffix(" )");
        String result = df.format(jj);
        if( (getKode()==null || getKode().trim().equals("")) && jj == 0 ) return " ";
        return result;
    }
    
    
    
}
