/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.web.laporan.apbd;

import java.io.Serializable;

/**
 *
 * @author detra
 */
public class DaftarPegawaiObj implements Serializable{
    String golongan;
    int jumlahEselon1;
    int jumlahEselon2;
    int jumlahEselon3;
    int jumlahEselon4;
    int jumlahEselon5;
    int jumlahFungsional;
    int jumlahStaf;
    String fontStyle;

    public DaftarPegawaiObj() {
    }

    public DaftarPegawaiObj(String golongan, int jumlahEselon1, int jumlahEselon2, int jumlahEselon3, int jumlahEselon4, int jumlahEselon5, int jumlahFungsional, int jumlahStaf) {
        this.golongan = golongan;
        this.jumlahEselon1 = jumlahEselon1;
        this.jumlahEselon2 = jumlahEselon2;
        this.jumlahEselon3 = jumlahEselon3;
        this.jumlahEselon4 = jumlahEselon4;
        this.jumlahEselon5 = jumlahEselon5;
        this.jumlahFungsional = jumlahFungsional;
        this.jumlahStaf = jumlahStaf;
    }

    public String getGolongan() {
        return golongan;
    }

    public void setGolongan(String golongan) {
        this.golongan = golongan;
    }

    public int getJumlahEselon1() {
        return jumlahEselon1;
    }

    public void setJumlahEselon1(int jumlahEselon1) {
        this.jumlahEselon1 = jumlahEselon1;
    }

    public int getJumlahEselon2() {
        return jumlahEselon2;
    }

    public void setJumlahEselon2(int jumlahEselon2) {
        this.jumlahEselon2 = jumlahEselon2;
    }

    public int getJumlahEselon3() {
        return jumlahEselon3;
    }

    public void setJumlahEselon3(int jumlahEselon3) {
        this.jumlahEselon3 = jumlahEselon3;
    }

    public int getJumlahEselon4() {
        return jumlahEselon4;
    }

    public void setJumlahEselon4(int jumlahEselon4) {
        this.jumlahEselon4 = jumlahEselon4;
    }

    public int getJumlahEselon5() {
        return jumlahEselon5;
    }

    public void setJumlahEselon5(int jumlahEselon5) {
        this.jumlahEselon5 = jumlahEselon5;
    }

    public int getJumlahFungsional() {
        return jumlahFungsional;
    }

    public void setJumlahFungsional(int jumlahFungsional) {
        this.jumlahFungsional = jumlahFungsional;
    }

    public int getJumlahStaf() {
        return jumlahStaf;
    }

    public void setJumlahStaf(int jumlahStaf) {
        this.jumlahStaf = jumlahStaf;
    }
    
    public String getFontStyle() {
        if(fontStyle==null || fontStyle.trim().equals("")) fontStyle="font-weight: normal;";
        return fontStyle;
    }

    public void setFontStyle(String fontStyle) {
        this.fontStyle = fontStyle;
    }
    
    
    
}
