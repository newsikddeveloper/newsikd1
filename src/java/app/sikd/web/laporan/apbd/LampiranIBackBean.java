/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.laporan.apbd;

import app.sikd.entity.apbd.LampiranIPerdaAPBD;
import app.sikd.web.common.bean.ACOAFilteredBackBean;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Nongky
 */
@ManagedBean(name = "apbdLampiranI")
@ViewScoped
public class LampiranIBackBean extends ACOAFilteredBackBean {

    private LampiranIReportWorker reportWorker;

    @PostConstruct
    @Override
    protected void init() {
        super.init();
        reportWorker = new LampiranIReportWorker();
    }

    private List<LampiranIPerdaAPBD> reports;

    @Override
    public void onChangeProcess() {
        try {
            reports = reportWorker.updateLampiranIReport(new Short(year), pemda, new Short(coaType));
        } catch (Exception ex) {
            Logger.getLogger(LampiranIBackBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<LampiranIPerdaAPBD> getData() {
        return reports;
    }
}
