/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.laporan.apbd;

import app.sikd.util.SIKDUtil;
import app.sikd.web.common.bean.ABasicFilteredBackBean;
import app.sikd.web.common.bean.ISKPDFilteredBean;
import app.sikd.web.common.filter.SKPDFilter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Nongky
 */
@ManagedBean(name = "apbdLampiranIII")
@ViewScoped
public class LampiranIIIBackBean extends ABasicFilteredBackBean implements ISKPDFilteredBean {

    private LampiranIIIReportWorker reportWorker;
    private SKPDFilter skpdFilter;
    private String skpd;

    @PostConstruct
    @Override
    protected void init() {
        super.init();
        additionalRender = ":lampiran3-data-filter-form:skpd-label-som";
        reportWorker = new LampiranIIIReportWorker();
        skpdFilter = new SKPDFilter();
    }

    private List<LampiranIIIAPBDObj> reports;

    @Override
    public void onChangeProcess() {
        try {
            reports = reportWorker.updateLampiranIIIReport(new Short(year), pemda, skpd, SIKDUtil.COA_PMDN13);
        } catch (Exception ex) {
            Logger.getLogger(LampiranIIIBackBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<LampiranIIIAPBDObj> getData() {
        return reports;
    }

    @Override
    public void onFilterChanged() {
        if (skpd != null && skpd.length() > 0) {
            onChangeProcess();
        } else {
            if (pemda != null && (year != null && year.length() > 0)) {
                try {
                    skpdList = skpdFilter.getSKPDs(new Short(year), pemda.getKodeSatker(), SIKDUtil.APBD_MURNI_SHORT, (short) 1);
                } catch (Exception ex) {
                    Logger.getLogger(LampiranIIIBackBean.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                if (skpdList != null) {
                    skpdList.clear();
                }
            }
        }
    }

    private List<String> skpdList;

    @Override
    public List<String> getSKPDs() {
        return skpdList;
    }

    @Override
    public void setSelectedSKPD(String _skpd) {
        skpd = _skpd;
    }

    @Override
    public String getSelectedSKPD() {
        return skpd;
    }

}
