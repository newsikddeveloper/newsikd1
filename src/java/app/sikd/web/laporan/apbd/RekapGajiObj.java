/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.web.laporan.apbd;

import java.io.Serializable;

/**
 *
 * @author sora
 */
public class RekapGajiObj implements Serializable{
    private String golongan;
    private String jumlah;
    private String gaji;
    private String tunjangan1;
    private String tunjangan2;
    private String tunjangan3;
    private String potongan1;
    private String potongan2;
    private String jumlahBersih;
    private String style;

    public RekapGajiObj() {
    }
    public RekapGajiObj(String golongan) {
        this.golongan = golongan;
    }

    public RekapGajiObj(String golongan, String jumlah, String gaji, String tunjangan1, String tunjangan2, String tunjangan3, String potongan1, String potongan2, String jumlahBersih, String keterangan, String style) {
        this.golongan = golongan;
        this.jumlah = jumlah;
        this.gaji = gaji;
        this.tunjangan1 = tunjangan1;
        this.tunjangan2 = tunjangan2;
        this.tunjangan3 = tunjangan3;
        this.potongan1 = potongan1;
        this.potongan2 = potongan2;
        this.jumlahBersih = jumlahBersih;
        this.keterangan = keterangan;
        this.style = style;
    }
    
    private String keterangan;

    public String getGolongan() {
        return golongan;
    }

    public void setGolongan(String golongan) {
        this.golongan = golongan;
    }

    public String getJumlah() {
        return jumlah;
    }

    public void setJumlah(String jumlah) {
        this.jumlah = jumlah;
    }

    public String getGaji() {
        return gaji;
    }

    public void setGaji(String gaji) {
        this.gaji = gaji;
    }

    public String getTunjangan1() {
        return tunjangan1;
    }

    public void setTunjangan1(String tunjangan1) {
        this.tunjangan1 = tunjangan1;
    }

    public String getTunjangan2() {
        return tunjangan2;
    }

    public void setTunjangan2(String tunjangan2) {
        this.tunjangan2 = tunjangan2;
    }

    public String getTunjangan3() {
        return tunjangan3;
    }

    public void setTunjangan3(String tunjangan3) {
        this.tunjangan3 = tunjangan3;
    }

    public String getPotongan1() {
        return potongan1;
    }

    public void setPotongan1(String potongan1) {
        this.potongan1 = potongan1;
    }

    public String getPotongan2() {
        return potongan2;
    }

    public void setPotongan2(String potongan2) {
        this.potongan2 = potongan2;
    }

    public String getJumlahBersih() {
        return jumlahBersih;
    }

    public void setJumlahBersih(String jumlahBersih) {
        this.jumlahBersih = jumlahBersih;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }
    
    
    
    
}
