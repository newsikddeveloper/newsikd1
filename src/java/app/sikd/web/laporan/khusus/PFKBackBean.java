/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.laporan.khusus;

import app.sikd.web.common.bean.ABasicFilteredBackBean;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Nongky
 */
@ManagedBean(name = "pfkBackBean")
@ViewScoped
public class PFKBackBean extends ABasicFilteredBackBean {

    private PFKReportWorker reportWorker;

    @PostConstruct
    protected void init() {
        super.init();
        reportWorker = new PFKReportWorker();
        setPage("/Laporan/Khusus/pfk/pfk-content.xhtml");
    }

    private List<RincianPFKObj> dataList;

    public List<RincianPFKObj> getData() {
        return dataList;
    }

    @Override
    protected void onChangeProcess() {
        try {
            dataList = reportWorker.updatePFKReport(new Short(year), pemda.getKodeSatker());
        } catch (Exception ex) {
            Logger.getLogger(PFKBackBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
