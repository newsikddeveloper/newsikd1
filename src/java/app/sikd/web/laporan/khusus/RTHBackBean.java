/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.laporan.khusus;

import app.sikd.entity.backoffice.RTH;
import app.sikd.web.common.bean.ABasicFilteredBackBean;
import app.sikd.web.common.bean.IPeriodFilteredBean;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Nongky
 */
@ManagedBean(name = "rthBackBean")
@ViewScoped
public class RTHBackBean extends ABasicFilteredBackBean implements IPeriodFilteredBean {

    private String periode;

    private RTHReportWorker reportWorker;

    @PostConstruct
    @Override
    protected void init() {
        super.init();
        reportWorker = new RTHReportWorker();
    }

    private List<RTH> rths;

    public List<RTH> getData() {
        if (rths == null) {
            new ArrayList<RTH>();
        }
        return rths;
    }

    @Override
    public void setPeriode(String _periode) {
        periode = _periode;
    }

    @Override
    public String getPeriode() {
        return periode;
    }

    @Override
    protected void onChangeProcess() {
        if (periode != null && periode.length() > 0) {
            try {
                rths = reportWorker.updateRTHReport(new Short(year), pemda.getKodeSatker(), new Short(periode));
            } catch (Exception ex) {
                Logger.getLogger(RTHBackBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
