/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.laporan.khusus;

import app.sikd.web.common.bean.ABasicFilteredBackBean;
import app.sikd.web.common.bean.IPeriodFilteredBean;
import app.sikd.web.common.bean.ISKPDFilteredBean;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Nongky
 */
@ManagedBean(name = "dthBackBean")
@ViewScoped
public class DTHBackBean extends ABasicFilteredBackBean implements ISKPDFilteredBean, IPeriodFilteredBean {

    private String skpd;
    private String periode;
    private DTHReportWorker reportWorker;

    @PostConstruct
    @Override
    protected void init() {
        super.init();
        reportWorker = new DTHReportWorker();
        setAdditionalRender(":dth-data-filter-form:skpd-label-som");
    }

    private List<DTHObj> rths;

    public List<DTHObj> getData() {
        if (rths == null) {
            new ArrayList<DTHObj>();
        }
        return rths;
    }

    @Override
    public void onFilterChanged() {

        if ((skpd != null && skpd.length() > 0) && (periode != null && periode.length() > 0)) {
            onChangeProcess();
        } else {
            if (pemda != null && (year != null && year.length() > 0)) {
                try {
                    reportWorker.updateSKPDList(new Short(year), pemda.getKodeSatker());
                    skpdList = reportWorker.getSkpds();
                } catch (Exception ex) {
                    Logger.getLogger(DTHBackBean.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    private List<String> skpdList;

    @Override
    public List<String> getSKPDs() {
        return skpdList;
    }

    @Override
    public void setSelectedSKPD(String _skpd) {
        skpd = _skpd;
    }

    @Override
    public String getSelectedSKPD() {
        return skpd;
    }

    @Override
    public void setPeriode(String _periode) {
        periode = _periode;
    }

    @Override
    public String getPeriode() {
        return periode;
    }

    @Override
    protected void onChangeProcess() {
        try {
            rths = reportWorker.updateDTHReport(new Short(year), pemda.getKodeSatker(), skpd, new Short(periode));
        } catch (Exception ex) {
            Logger.getLogger(DTHBackBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
