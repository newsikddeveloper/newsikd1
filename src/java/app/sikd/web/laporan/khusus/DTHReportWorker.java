/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.laporan.khusus;

import app.sikd.aas.ServerUtil;
import app.sikd.entity.backoffice.DTH;
import app.sikd.entity.backoffice.RincianDTHSKPD;
import app.sikd.office.ejb.session.PajakSessionBeanRemote;
import app.sikd.util.SIKDUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;

/**
 *
 * @author Nongky
 */
public class DTHReportWorker {

    PajakSessionBeanRemote pajakService;
    private List<DTHObj> dths = new ArrayList<>();
    private List<String> skpds = new ArrayList<>();
    String kodeSKPD, namaSKPD, labelMonth;

    public DTHReportWorker() {
        try {
            Context context = ServerUtil.getOfficeContext();
            pajakService = (PajakSessionBeanRemote) context.lookup("PajakSessionBean/remote");
        } catch (Exception ex) {
            Logger.getLogger(DTHReportWorker.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void updateSKPDList(short _year, String _kodeSatker) throws Exception {
        if (skpds != null) {
            skpds.clear();
        }
        skpds = pajakService.getDinasKirimDTH(_year, _kodeSatker);
    }

    public List<DTHObj> updateDTHReport(short _year, String _kodeSatker, String _skpd, short _periode) throws Exception {
        kodeSKPD = "";
        namaSKPD = "";

        if (_skpd != null && !_skpd.trim().equals("") && !_skpd.trim().equals("0")) {
            java.util.StringTokenizer tok = new java.util.StringTokenizer(_skpd, " ");
            if (tok.countTokens() > 0) {
                kodeSKPD = tok.nextToken();
            }
            namaSKPD = _skpd.substring(kodeSKPD.trim().length(), _skpd.trim().length());
        }
        labelMonth = "";
//        if (_periode > 0 && _periode() <= 12) {
//            labelMonth = ServerUtilgetMonthAsString(_periode);
//        }

        if (kodeSKPD != null && kodeSKPD.trim().length() > 6) {
            DTH ls = pajakService.getDTHReport(_year, _kodeSatker, _periode, kodeSKPD);
            dths.clear();
            int no = 1, indexDTHO;
            if (ls != null && ls.getDthSkpd() != null && ls.getDthSkpd().size() > 0 && ls.getDthSkpd().get(0) != null && ls.getDthSkpd().get(0).getRincians() != null) {
                List<RincianDTHSKPD> rincians = ls.getDthSkpd().get(0).getRincians();
                for (RincianDTHSKPD r : rincians) {
                    indexDTHO = dths.size();
                    double jumlah = 0;
                    DTHObj dtho = new DTHObj(String.valueOf(no), r.getNomorSPM(), "", r.getNomorSP2D(), "", SIKDUtil.getSumberDanaSP2DAsString(r.getSumberDanaSP2D()), r.getKeterangan());
                    no++;
                    dtho.setNpwpBUD(r.getNpwpBUD());
                    dtho.setNpwpSKPD(r.getNpwpSKPD());
                    dtho.setNpwpVendor(r.getNpwpVendor());
                    dtho.setNamaVendor(r.getNamaVendor());
                    if ((r.getAkuns() != null && r.getAkuns().size() > 0) || (r.getPajaks() != null && r.getPajaks().size() > 0)
                            || (r.getPotongans() != null && r.getPotongans().size() > 0)) {
                        int len = 0;
                        if (r.getAkuns() != null) {
                            len = r.getAkuns().size();
                        }
                        if (r.getPajaks() != null) {
                            if (r.getPajaks().size() > len) {
                                len = r.getPajaks().size();
                            }
                        }

                        for (int i = 0; i < len; i++) {
                            if (i > 0) {
                                dtho = new DTHObj("", "", "", "", "", "", "");
                            }
                            if (r.getAkuns() != null) {
                                if (r.getAkuns().size() > i) {
                                    dtho.setKodeAkun(r.getAkuns().get(i).getKodeAkunUtama());
                                    jumlah += r.getAkuns().get(i).getNilai();
                                }
                            }
                            if (r.getPajaks() != null) {
                                if (r.getPajaks().size() > i) {
                                    dtho.setAkunPajak(r.getPajaks().get(i).getKodeAkunPajak());
                                    dtho.setJenisPajak(SIKDUtil.getJenisPajakAsString(r.getPajaks().get(i).getJenisPajak()));
                                    dtho.setJumlahPajak(SIKDUtil.doubleToString(r.getPajaks().get(i).getNilaiPotongan()));
                                }
                            }

                            if (r.getPotongans() != null) {
                                if (r.getPotongans().size() > i) {
                                    dtho.setAkunPotongan(r.getPotongans().get(i).getKodeAkunPotongan());
                                    dtho.setJenisPotongan(SIKDUtil.getJenisPotonganAsString(r.getPotongans().get(i).getJenisPotongan()));
                                    dtho.setJumlahPotongan(SIKDUtil.doubleToString(r.getPotongans().get(i).getNilaiPotongan()));
                                }
                            }
//                            if( i == 0 ){
                            dths.add(dtho);
//                            }
                        }
                        dths.get(indexDTHO).setNilaiSP2D(SIKDUtil.doubleToString(jumlah));
                        dths.get(indexDTHO).setNilaiSPM(SIKDUtil.doubleToString(jumlah));
                    }
                }
            }
        }
        return dths;
    }

    public List<String> getSkpds() {
        return skpds;
    }

}
