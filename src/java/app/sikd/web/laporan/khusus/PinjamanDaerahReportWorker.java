/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.laporan.khusus;

import app.sikd.aas.ServerUtil;
import app.sikd.entity.backoffice.RincianPinjamanDaerah;
import app.sikd.office.ejb.session.APBDSessionBeanRemote;
import app.sikd.web.common.filter.NeracaLabel;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;

/**
 *
 * @author Nongky
 */
public class PinjamanDaerahReportWorker {

    private APBDSessionBeanRemote service;

    public PinjamanDaerahReportWorker() {
        try {
            Context context = ServerUtil.getOfficeContext();
            service = (APBDSessionBeanRemote) context.lookup("APBDSessionBean/remote");
        } catch (Exception ex) {
            Logger.getLogger(NeracaLabel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<RincianPinjamanDaerahObj> updatePinjamanDaerahReport(short _year, String _kodeSatker) throws Exception {
        List<RincianPinjamanDaerahObj> objects = new ArrayList<>();
        List<RincianPinjamanDaerah> rpd = service.getRincianPinjamanDaerah(_year, _kodeSatker);
        if (rpd != null && rpd.size() > 0) {
            for (RincianPinjamanDaerah rpd1 : rpd) {
                objects.add(new RincianPinjamanDaerahObj("", rpd1.getSumber(), rpd1.getDasarHukum(), rpd1.getTanggalPerjanjian(), rpd1.getJumlahPinjaman(), rpd1.getJangkaWaktu(), rpd1.getBunga(), rpd1.getTujuan(), rpd1.getBayarPokok(), rpd1.getBayarBunga(), rpd1.getSisaPokok(), rpd1.getSisaBunga()));
            }
        }
        return objects;
    }

}
