/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.laporan.khusus;

import app.sikd.aas.ServerUtil;
import app.sikd.entity.backoffice.RTH;
import app.sikd.office.ejb.session.PajakSessionBeanRemote;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;

/**
 *
 * @author Nongky
 */
public class RTHReportWorker {

    PajakSessionBeanRemote pajakService;
    private List<RTH> rths = new ArrayList<>();
    String labelMonth;

    public RTHReportWorker() {
        try {
            Context context = ServerUtil.getOfficeContext();
            pajakService = (PajakSessionBeanRemote) context.lookup("PajakSessionBean/remote");
        } catch (Exception ex) {
            Logger.getLogger(RTHReportWorker.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<RTH> updateRTHReport(short _year, String _kodeSatker, short _periode) throws Exception {
        labelMonth = "";
//        if (_periode > 0 && _periode() <= 12) {
//            labelMonth = ServerUtilgetMonthAsString(_periode);
//        }
        rths.clear();
        rths = pajakService.getRTHReport(_year, _kodeSatker, _periode);
        return rths;
    }

}
