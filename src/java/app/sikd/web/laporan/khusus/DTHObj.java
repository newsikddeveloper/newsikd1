/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.web.laporan.khusus;

import java.io.Serializable;

/**
 *
 * @author sora
 */
public class DTHObj implements Serializable {
    private String noUrut;
    private String nomorSPM;
    private String nilaiSPM;
    private String nomorSP2D;
    private String nilaiSP2D;
    private String sumberDana;
    private String kodeAkun;
        
    private String akunPajak;
    private String jenisPajak;
    private String jumlahPajak;
    
    private String akunPotongan;
    private String jenisPotongan;
    private String jumlahPotongan;
    
    private String npwpBUD;
    private String npwpSKPD;
    private String npwpVendor;
    private String namaVendor;
    private String keterangan;

    public DTHObj() {
    }

    public DTHObj(String noUrut, String nomorSPM, String nilaiSPM, String nomorSP2D, String nilaiSP2D, String sumberDana, String keterangan) {
        this.noUrut = noUrut;
        this.nomorSPM = nomorSPM;
        this.nilaiSPM = nilaiSPM;
        this.nomorSP2D = nomorSP2D;
        this.nilaiSP2D = nilaiSP2D; 
        this.sumberDana = sumberDana;
        this.keterangan = keterangan;
    }
    public DTHObj(String noUrut, String nomorSPM, String nilaiSPM, String nomorSP2D, String nilaiSP2D, String sumberDana, 
            String kodeAkun, String akunPajak, String jenisPajak, String jumlahPajak, 
            String akunPotongan, String jenisPotongan, String jumlahPotongan, 
            String npwpBUD, String npwpSKPD, String npwpVendor, String namaVendor, String keterangan) {
        this.noUrut = noUrut;
        this.nomorSPM = nomorSPM;
        this.nilaiSPM = nilaiSPM;
        this.nomorSP2D = nomorSP2D;
        this.nilaiSP2D = nilaiSP2D;
        this.sumberDana = sumberDana;
        this.kodeAkun = kodeAkun;
        this.akunPajak = akunPajak;
        this.jenisPajak = jenisPajak;
        this.jumlahPajak = jumlahPajak;
        this.akunPotongan = akunPotongan;
        this.jenisPotongan = jenisPotongan;
        this.jumlahPotongan = jumlahPotongan;
        this.npwpBUD = npwpBUD;
        this.npwpSKPD = npwpSKPD;
        this.npwpVendor = npwpVendor;
        this.namaVendor = namaVendor;
        this.keterangan = keterangan;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getNoUrut() {
        return noUrut;
    }

    public void setNoUrut(String noUrut) {
        this.noUrut = noUrut;
    }

    public String getNomorSPM() {
        return nomorSPM;
    }

    public void setNomorSPM(String nomorSPM) {
        this.nomorSPM = nomorSPM;
    }

    public String getNilaiSPM() {
        return nilaiSPM;
    }

    public void setNilaiSPM(String nilaiSPM) {
        this.nilaiSPM = nilaiSPM;
    }

    public String getNomorSP2D() {
        return nomorSP2D;
    }

    public void setNomorSP2D(String nomorSP2D) {
        this.nomorSP2D = nomorSP2D;
    }

    public String getNilaiSP2D() {
        return nilaiSP2D;
    }

    public void setNilaiSP2D(String nilaiSP2D) {
        this.nilaiSP2D = nilaiSP2D;
    }

    public String getKodeAkun() {
        return kodeAkun;
    }

    public void setKodeAkun(String kodeAkun) {
        this.kodeAkun = kodeAkun;
    }

    public String getAkunPajak() {
        return akunPajak;
    }

    public void setAkunPajak(String akunPajak) {
        this.akunPajak = akunPajak;
    }

    public String getJenisPajak() {
        return jenisPajak;
    }

    public void setJenisPajak(String jenisPajak) {
        this.jenisPajak = jenisPajak;
    }

    public String getJumlahPajak() {
        return jumlahPajak;
    }

    public void setJumlahPajak(String jumlahPajak) {
        this.jumlahPajak = jumlahPajak;
    }

    public String getSumberDana() {
        return sumberDana;
    }

    public void setSumberDana(String sumberDana) {
        this.sumberDana = sumberDana;
    }

    public String getAkunPotongan() {
        return akunPotongan;
    }

    public void setAkunPotongan(String akunPotongan) {
        this.akunPotongan = akunPotongan;
    }

    public String getJenisPotongan() {
        return jenisPotongan;
    }

    public void setJenisPotongan(String jenisPotongan) {
        this.jenisPotongan = jenisPotongan;
    }

    public String getJumlahPotongan() {
        return jumlahPotongan;
    }

    public void setJumlahPotongan(String jumlahPotongan) {
        this.jumlahPotongan = jumlahPotongan;
    }

    public String getNpwpBUD() {
        return npwpBUD;
    }

    public void setNpwpBUD(String npwpBUD) {
        this.npwpBUD = npwpBUD;
    }

    public String getNpwpSKPD() {
        return npwpSKPD;
    }

    public void setNpwpSKPD(String npwpSKPD) {
        this.npwpSKPD = npwpSKPD;
    }

    public String getNpwpVendor() {
        return npwpVendor;
    }

    public void setNpwpVendor(String npwpVendor) {
        this.npwpVendor = npwpVendor;
    }

    public String getNamaVendor() {
        return namaVendor;
    }

    public void setNamaVendor(String namaVendor) {
        this.namaVendor = namaVendor;
    }
    
}