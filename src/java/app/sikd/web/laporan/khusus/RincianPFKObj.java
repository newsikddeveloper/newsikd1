/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.web.laporan.khusus;

import app.sikd.entity.backoffice.RincianPerhitunganFihakKetiga;
import app.sikd.util.SIKDUtil;
import java.io.Serializable;

/**
 *
 * @author sora
 */
public class RincianPFKObj extends RincianPerhitunganFihakKetiga implements Serializable{
    private String no;
    private double selisih;
    String style;

    public RincianPFKObj(String style, String no, String uraian, double pungutan, double setoran) {
        super(uraian, pungutan, setoran);
        this.no = no;
        selisih = pungutan - setoran;
        this.style = style;
    }

    public RincianPFKObj() {
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public double getSelisih() {
        return (getPungutan()-getSetoran());        
    }
    
    public String getSelisihAsString(){
        return SIKDUtil.doubleToString(getPungutan()-getSetoran());
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }
    
    
    
    
}
