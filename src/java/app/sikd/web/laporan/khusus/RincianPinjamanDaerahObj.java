/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.web.laporan.khusus;

import app.sikd.entity.backoffice.RincianPinjamanDaerah;
import app.sikd.entity.ws.fault.SIKDServiceException;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author sora
 */
public class RincianPinjamanDaerahObj extends RincianPinjamanDaerah implements Serializable{
    private String style;

    public RincianPinjamanDaerahObj() {
    }

    public RincianPinjamanDaerahObj(String sumber, String dasarHukum, Date tanggalPerjanjian, double jumlahPinjaman, double jangkaWaktu, double bunga, String tujuan, double bayarPokok, double bayarBunga, double sisaPokok, double sisaBunga) throws SIKDServiceException {
        super(sumber, dasarHukum, tanggalPerjanjian, jumlahPinjaman, jangkaWaktu, bunga, tujuan, bayarPokok, bayarBunga, sisaPokok, sisaBunga);
    }

    public RincianPinjamanDaerahObj(String style, String sumber, String dasarHukum, Date tanggalPerjanjian, double jumlahPinjaman, double jangkaWaktu, double bunga, String tujuan, double bayarPokok, double bayarBunga, double sisaPokok, double sisaBunga) throws SIKDServiceException {
        super(sumber, dasarHukum, tanggalPerjanjian, jumlahPinjaman, jangkaWaktu, bunga, tujuan, bayarPokok, bayarBunga, sisaPokok, sisaBunga);
        this.style = style;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }
    
    
}
