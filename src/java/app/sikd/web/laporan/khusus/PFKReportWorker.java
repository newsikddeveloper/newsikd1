/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.laporan.khusus;

import app.sikd.aas.ServerUtil;
import app.sikd.entity.backoffice.RincianPerhitunganFihakKetiga;
import app.sikd.office.ejb.session.APBDSessionBeanRemote;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;

/**
 *
 * @author Nongky
 */
public class PFKReportWorker {

    private APBDSessionBeanRemote service;

    public PFKReportWorker() {
        try {
            Context context = ServerUtil.getOfficeContext();
            service = (APBDSessionBeanRemote) context.lookup("APBDSessionBean/remote");
        } catch (Exception ex) {
            Logger.getLogger(PFKReportWorker.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<RincianPFKObj> updatePFKReport(short _year, String _kodeSatker) throws Exception {
        List<RincianPFKObj> objects = new ArrayList<>();
        List<RincianPerhitunganFihakKetiga> rpfk = service.getRincianPerhitunganFihakKetiga(_year, _kodeSatker);
        if (rpfk != null && rpfk.size() > 0) {
            int i = 0;
            for (RincianPerhitunganFihakKetiga rpfk1 : rpfk) {
                i++;
                objects.add(new RincianPFKObj("", String.valueOf(i), rpfk1.getUraian(), rpfk1.getPungutan(), rpfk1.getSetoran()));
            }
        }
        return objects;
    }

}
