/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.laporan.lkpdakrual;

import app.sikd.aas.ServerUtil;
import app.sikd.entity.backoffice.LOReport;
import app.sikd.office.ejb.session.LaporanSessionBeanRemote;
import app.sikd.web.common.filter.NeracaLabel;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;

/**
 *
 * @author Nongky
 */
public class OperasionalReportWorker {

    private LaporanSessionBeanRemote service;

    public OperasionalReportWorker() {
        try {
            Context context = ServerUtil.getOfficeContext();
            service = (LaporanSessionBeanRemote) context.lookup("LaporanSessionBean/remote");
        } catch (Exception ex) {
            Logger.getLogger(NeracaLabel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<LOReport> updateOperasionalReport(short _year, short _triwulan, String _kodeSatker) throws Exception {
        List<LOReport> los = service.getLaporanOperasionalReports(_year, _triwulan, _kodeSatker);
        return los;
    }

}
