/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.laporan.lkpdakrual;

import app.sikd.entity.backoffice.LOReport;
import app.sikd.web.common.bean.ABasicFilteredBackBean;
import app.sikd.web.common.bean.ITriwulanFilteredBean;
import app.sikd.web.common.filter.TriwulanFilter;
import app.sikd.web.laporan.lkpd.NeracaBackBean;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Nongky
 */
@ViewScoped
@ManagedBean(name = "laporanOperasionalBean")
public class LaporanOperasionalBackBean extends ABasicFilteredBackBean implements ITriwulanFilteredBean {

    private TriwulanFilter triwulanFilter;
    private OperasionalReportWorker reportWorker;
    private String triwulan;

    @PostConstruct
    @Override
    protected void init() {
        super.init();
        triwulanFilter = new TriwulanFilter();
        reportWorker = new OperasionalReportWorker();
        setPage("/Laporan/LKPDAkrual/lo/lo-content.xhtml");
    }

    @Override
    public List<String> getTriwulans() {
        return triwulanFilter.getTriwulans();
    }

    @Override
    public void setSelectedTriwulan(String _triwulan) {
        triwulan = _triwulan;
    }

    @Override
    public String getSelectedTriwulan() {
        return triwulan;
    }

    private List<LOReport> reports;

    public List<LOReport> getData() {
        return reports;
    }

    @Override
    protected void onChangeProcess() {
        try {
            reports = reportWorker.updateOperasionalReport(new Short(year), new Short(triwulan), pemda.getKodeSatker());
        } catch (Exception ex) {
            Logger.getLogger(NeracaBackBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void onFilterChanged() {
        if (pemda != null
                && (year != null && year.length() > 0)
                && (triwulan != null && triwulan.length() > 0)) {
            onChangeProcess();
        }
    }

}
