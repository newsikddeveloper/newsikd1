/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.laporan.lkpdakrual;

import app.sikd.web.common.bean.ABasicFilteredBackBean;
import app.sikd.web.laporan.lkpd.NeracaBackBean;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Nongky
 */
@ViewScoped
@ManagedBean(name = "perubahanSALBean")
public class LaporanPerubahanSALBackBean extends ABasicFilteredBackBean {

    private PerubahanSALReportWorker reportWorker;

    @PostConstruct
    @Override
    protected void init() {
        super.init();
        reportWorker = new PerubahanSALReportWorker();
        setPage("/Laporan/LKPDAkrual/lpsal/lpsal-content.xhtml");
    }

    private List<SalObj> reports;

    public List<SalObj> getData() {
        return reports;
    }

    @Override
    protected void onChangeProcess() {
        try {
            reports = reportWorker.updatePerubahanSALReport(new Short(year), pemda);
        } catch (Exception ex) {
            Logger.getLogger(NeracaBackBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
