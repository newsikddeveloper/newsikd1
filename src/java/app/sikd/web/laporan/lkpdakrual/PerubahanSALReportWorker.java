/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.laporan.lkpdakrual;

import app.sikd.aas.ServerUtil;
import app.sikd.entity.Pemda;
import app.sikd.entity.backoffice.PerubahanSAL;
import app.sikd.office.ejb.session.LaporanSessionBeanRemote;
import app.sikd.util.SIKDUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;

/**
 *
 * @author Nongky
 */
public class PerubahanSALReportWorker {

    private LaporanSessionBeanRemote service;

    public PerubahanSAL selectedSAL, editedSAL;

    public PerubahanSALReportWorker() {
        try {
            Context context = ServerUtil.getOfficeContext();
            service = (LaporanSessionBeanRemote) context.lookup("LaporanSessionBean/remote");
        } catch (Exception ex) {
            Logger.getLogger(PerubahanSALReportWorker.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public PerubahanSAL getSelectedSAL() {
        return selectedSAL;
    }

    public void setSelectedSAL(PerubahanSAL _selectedSAL) {
        this.selectedSAL = _selectedSAL;
    }

    public PerubahanSAL getEditedSAL() {
        return editedSAL;
    }

    public void setEditedSAL(PerubahanSAL editedSAL) {
        this.editedSAL = editedSAL;
    }

    private List<SalObj> sals = new ArrayList();

    public List<SalObj> updatePerubahanSALReport(short _year, Pemda _pemda) throws Exception {
        sals.clear();
        selectedSAL = service.getPerubahanSalReports(_year, _pemda.getKodeSatker());
        if (selectedSAL == null) {
            selectedSAL = new PerubahanSAL();
            selectedSAL.setTahunAnggaran(_year);
            selectedSAL.setKodeSatker(_pemda.getKodeSatker());
        }
        editedSAL = new PerubahanSAL(selectedSAL.getIndex(), selectedSAL.getKodeSatker(), selectedSAL.getKodePemda(), selectedSAL.getNamaPemda(), selectedSAL.getTahunAnggaran(), selectedSAL.getSalAwal(), selectedSAL.getPenggunaanSal(), selectedSAL.getSilpa(), selectedSAL.getKoreksi(), selectedSAL.getLainLain());

        if (editedSAL.getIndex() <= 0) {
            editedSAL.setKodePemda(_pemda.getKodePemda());
            editedSAL.setNamaPemda(_pemda.getNamaPemda());
        }

        PerubahanSAL salMin = service.getPerubahanSalReports((short) (_year - 1), _pemda.getKodeSatker());
        if (salMin == null) {
            salMin = new PerubahanSAL();
        }

        double sub1 = selectedSAL.getSalAwal() + selectedSAL.getPenggunaanSal();
        double subMin = salMin.getSalAwal() + salMin.getPenggunaanSal();
        double silpa = sub1 + selectedSAL.getSilpa();
        double silpaMin = subMin + salMin.getSilpa();
        double tot = silpa + selectedSAL.getKoreksi() + selectedSAL.getLainLain();
        double totMin = silpaMin + salMin.getKoreksi() + salMin.getLainLain();

        sals.add(new SalObj("1", "Saldo Anggaran Lebih Awal", (selectedSAL.getIndex() > 0) ? selectedSAL.getSalAwalAsString() : "", (salMin.getIndex() > 0) ? salMin.getSalAwalAsString() : "", ""));
        sals.add(new SalObj("2", "Penggunaan SAL sebagai Penerimaan Pembiayaan Tahun Berjalan", (selectedSAL.getIndex() > 0) ? selectedSAL.getPenggunaanSalAsString() : "", (salMin.getIndex() > 0) ? salMin.getPenggunaanSalAsString() : "", ""));
        sals.add(new SalObj("3", "Sub Total (1+2)", (selectedSAL.getIndex() > 0) ? SIKDUtil.doubleToString(sub1) : "", (salMin.getIndex() > 0) ? SIKDUtil.doubleToString(subMin) : "", "font-weight: bold;"));
        sals.add(new SalObj("", "", "", "", ""));
        sals.add(new SalObj("4", "Sisa Lebih/Kurang Pembiayaan Anggaran (SiLPA/SiKPA)", (selectedSAL.getIndex() > 0) ? selectedSAL.getSilpaAsString() : "", (salMin.getIndex() > 0) ? salMin.getSilpaAsString() : "", ""));
        sals.add(new SalObj("5", "Sub Total (3+4)", (selectedSAL.getIndex() > 0) ? SIKDUtil.doubleToString(silpa) : "", (salMin.getIndex() > 0) ? SIKDUtil.doubleToString(silpaMin) : "", "font-weight: bold;"));
        sals.add(new SalObj("", "", "", "", ""));
        sals.add(new SalObj("6", "Koreksi Kesalahan Pembukuan Tahun Sebelumnya", (selectedSAL.getIndex() > 0) ? selectedSAL.getKoreksiAsString() : "", (salMin.getIndex() > 0) ? salMin.getKoreksiAsString() : "", ""));
        sals.add(new SalObj("7", "Lain-lain", (selectedSAL.getIndex() > 0) ? selectedSAL.getLainLainAsString() : "", (salMin.getIndex() > 0) ? salMin.getLainLainAsString() : "", ""));
        sals.add(new SalObj("", "Saldo Anggaran Lebih Akhir (5+6+7)", (selectedSAL.getIndex() > 0) ? SIKDUtil.doubleToString(tot) : "", (salMin.getIndex() > 0) ? SIKDUtil.doubleToString(totMin) : "", "font-weight: bold; background-color: activecaption;"));
        return sals;
    }

}
