/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.laporan.lkpd;

import app.sikd.aas.ServerUtil;
import app.sikd.entity.backoffice.ArusKasReport;
import app.sikd.office.ejb.session.LaporanSessionBeanRemote;
import app.sikd.web.common.filter.NeracaLabel;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;

/**
 *
 * @author Nongky
 */
public class ArusKasReportWorker {

    private LaporanSessionBeanRemote service;

    public ArusKasReportWorker() {
        try {
            Context context = ServerUtil.getOfficeContext();
            service = (LaporanSessionBeanRemote) context.lookup("LaporanSessionBean/remote");
        } catch (Exception ex) {
            Logger.getLogger(NeracaLabel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<ArusKasReport> updateArusKasReport(short _year, String _kodeSatker, String _label) throws Exception {
        List<ArusKasReport> ls = service.getArusKasReports(_year, _kodeSatker, _label);
        return ls;
    }

}
