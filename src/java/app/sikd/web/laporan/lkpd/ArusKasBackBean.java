/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.laporan.lkpd;

import app.sikd.entity.backoffice.ArusKasReport;
import app.sikd.web.common.bean.AArusKasBackBean;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Nongky
 */
@ViewScoped
@ManagedBean(name = "arusKasBean")
public class ArusKasBackBean extends AArusKasBackBean {

    private ArusKasReportWorker reportWorker;

    @PostConstruct
    @Override
    protected void init() {
        super.init();
        reportWorker = new ArusKasReportWorker();
        setPage("/Laporan/LKPD/aruskas/aruskas-content.xhtml");
    }

    @Override
    public void onChangeProcess() {
        try {
            reports = reportWorker.updateArusKasReport(new Short(year), pemda.getKodeSatker(), label);
        } catch (Exception ex) {
            Logger.getLogger(NeracaBackBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private List<ArusKasReport> reports;

    public List<ArusKasReport> getData() {
        return reports;
    }

}
