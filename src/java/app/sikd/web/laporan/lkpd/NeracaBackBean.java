/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.laporan.lkpd;

import app.sikd.web.common.bean.ANeracaBackBean;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Nongky
 */
@ManagedBean(name = "neracaBean")
@ViewScoped
public class NeracaBackBean extends ANeracaBackBean {

    private NeracaReportWorker reportWorker;

    @PostConstruct
    @Override
    protected void init() {
        super.init();
        reportWorker = new NeracaReportWorker();
        setPage("/Laporan/LKPD/neraca/neraca-content.xhtml");
    }

    private List<NeracaObj> reports;

    @Override
    public void onChangeProcess() {
        try {
            reports = reportWorker.updateNeracaReport(new Short(year), new Short(semester), pemda.getKodeSatker(), label);
        } catch (Exception ex) {
            Logger.getLogger(NeracaBackBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<NeracaObj> getData() {
        return reports;
    }

}
