/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.laporan.lkpd;

import app.sikd.aas.ServerUtil;
import app.sikd.entity.backoffice.NeracaReport;
import app.sikd.office.ejb.session.LaporanSessionBeanRemote;
import app.sikd.util.SIKDUtil;
import app.sikd.web.common.filter.NeracaLabel;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;

/**
 *
 * @author Nongky
 */
public class NeracaReportWorker {

    private LaporanSessionBeanRemote service;

    public NeracaReportWorker() {
        try {
            Context context = ServerUtil.getOfficeContext();
            service = (LaporanSessionBeanRemote) context.lookup("LaporanSessionBean/remote");
        } catch (Exception ex) {
            Logger.getLogger(NeracaLabel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<NeracaObj> updateNeracaReport(short _year, short _semester, String _kodeSatker, String _label) throws Exception {
        List<NeracaObj> neracas = new ArrayList<>();
        List<NeracaReport> ls = service.getNeracaReports(_year, _semester, _kodeSatker, _label);

        if (ls != null) {
            for (NeracaReport l : ls) {
                if (l.getType() == 0) {
                    neracas.add(new NeracaObj(l.getNamaAkun(), "", "", "font-weight: bold;"));
                } else if (l.getType() == 1) {
                    neracas.add(new NeracaObj(l.getNamaAkun(), SIKDUtil.doubleToString(l.getNilai()), SIKDUtil.doubleToString(l.getNilaiPra()), "font-weight: bold; "));
                } else if (l.getType() == 2) {
                    neracas.add(new NeracaObj(l.getNamaAkun(), SIKDUtil.doubleToString(l.getNilai()), SIKDUtil.doubleToString(l.getNilaiPra()), "font-weight: bold; background-color: activecaption;"));
                } else {
                    neracas.add(new NeracaObj(l.getNamaAkun(), SIKDUtil.doubleToString(l.getNilai()), SIKDUtil.doubleToString(l.getNilaiPra()), ""));
                }
            }
        }
        return neracas;
    }

}
