/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.laporan.lkpd;

import app.sikd.entity.apbd.LampiranILra;
import app.sikd.web.common.bean.ACOAFilteredBackBean;
import app.sikd.web.laporan.apbd.realisasi.RealisasiAPBDReportWorker;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Nongky
 */
@ManagedBean(name = "realisasiAPBDLKPD")
@ViewScoped
public class RealisasiAPBDLKPDBackBean extends ACOAFilteredBackBean {

    private RealisasiAPBDReportWorker reportWorker;

    @PostConstruct
    @Override
    protected void init() {
        super.init();
        reportWorker = new RealisasiAPBDReportWorker();
        setPage("/Laporan/LKPD/lra/lra-content.xhtml");
    }

    private List<LampiranILra> reportData;

    public List<LampiranILra> getData() {
        return reportData;
    }

    @Override
    protected void onChangeProcess() {
        try {
            reportData = reportWorker.updateRealisasiAPBDReport(new Short(year), pemda.getKodeSatker(), (short) 13, new Short(coaType));
        } catch (Exception ex) {
            Logger.getLogger(RealisasiAPBDLKPDBackBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
