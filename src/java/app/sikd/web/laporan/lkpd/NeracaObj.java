/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.web.laporan.lkpd;

import java.io.Serializable;


/**
 *
 * @author sora
 */
public class NeracaObj implements Serializable{    
    private String nama;
    private String nilai;
    private String nilaiPra;
    private String fontStyle;

    public NeracaObj() {
    }

    public NeracaObj(String nama, String nilai) {        
        this.nama = nama;
        this.nilai = nilai;
    }
    
    public NeracaObj(String nama, String nilai, String nilaiPra) {
        this.nama = nama;
        this.nilai = nilai;
        this.nilaiPra = nilaiPra;
    }
    public NeracaObj(String nama, String nilai, String nilaiPra, String fontStyle) {
        this.nama = nama;
        this.nilai = nilai;
        this.nilaiPra = nilaiPra;
        this.fontStyle = fontStyle;
    }

    public String getNilai() {
        return nilai;
    }

    public void setNilai(String nilai) {
        this.nilai = nilai;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNilaiPra() {
        return nilaiPra;
    }

    public void setNilaiPra(String nilaiPra) {
        this.nilaiPra = nilaiPra;
    }

    public String getFontStyle() {
        return fontStyle;
    }

    public void setFontStyle(String fontStyle) {
        this.fontStyle = fontStyle;
    }
    
    
}
