package app.sikd.web.fungsi;

import app.sikd.aas.ServerUtil;
import app.sikd.aas.SessionUtil;
import app.sikd.entity.Pemda;
import app.sikd.entity.backoffice.NilaiFungsiPemda;
import app.sikd.entity.graph.GraphGlobal;
import app.sikd.login.ejb.session.AdministrasiSessionBeanRemote;
import app.sikd.login.ejb.session.NewLoginSessionBeanRemote;
import app.sikd.office.ejb.session.GraphicSessionBeanRemote;
import app.sikd.util.SIKDUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.naming.Context;
import javax.servlet.http.HttpServletRequest;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.HorizontalBarChartModel;

/**
 *
 * @author PC04
 */
@ManagedBean(name = "pendidikanMBean")
@ViewScoped

public class PendidikanMBean implements Serializable {
    HorizontalBarChartModel barModel;
    ChartSeries chartSeries;
    List<String> years;
    String selectedYear;
    String labelYear;
    List<Pemda> provinces;
    List<SelectItem> selectItemProvinces;
    Pemda selectedProvince;

    AdministrasiSessionBeanRemote administrasiSession;
    NewLoginSessionBeanRemote loginSession;
    GraphicSessionBeanRemote graphSession;
    long grupId;
    
    String barHeight="400px;";
    private List<NilaiFungsiPemdaObj> fungsis = new ArrayList<>();
    String pemdaLabel="";

    @PostConstruct
    public void init() {
        try {
            labelYear = "";
            String username = SessionUtil.getCookieUsername();
            FacesContext context = FacesContext.getCurrentInstance();
            ExternalContext excontext = context.getExternalContext();
            HttpServletRequest httpServletRequest = (HttpServletRequest) excontext.getRequest();
            String path = httpServletRequest.getRequestURI();
            String namamenu = SIKDUtil.getNamaMenu(path);
            if (username == null) {
                throw new Exception("Identitas pengguna tidak bisa ditemukan atau sudah lewat masa sesi koneksi. "
                        + "Pengguna harus kembali melakukan login.");
            }
            String grupids = SessionUtil.getCookieGroupid();
            if (grupids != null) {
                grupId = Long.valueOf(grupids);
            }

            Context ctxLogin = ServerUtil.getLoginContext();
            Context ctxOffice = ServerUtil.getOfficeContext();
            administrasiSession = (AdministrasiSessionBeanRemote) ctxLogin.lookup("AdministrasiSessionBean/remote");
            loginSession = (NewLoginSessionBeanRemote) ctxLogin.lookup("NewLoginSessionBean/remote");
            graphSession = (GraphicSessionBeanRemote) ctxOffice.lookup("GraphicSessionBean/remote");

            chartSeries = new ChartSeries();
            chartSeries.setLabel("");
            chartSeries.set("", 0);

            barModel = new HorizontalBarChartModel();
            
            Axis yAxis = barModel.getAxis(AxisType.Y);
            yAxis.setTickCount(6);
            yAxis.setMax(1);

            barModel.addSeries(chartSeries);

            years = new ArrayList();
            provinces = new ArrayList();
            selectItemProvinces = new ArrayList();
            selectItemProvinces.add(new SelectItem(new Pemda("00", "00", "000000", "Nasional", "Nasional", (short)-1), "Nasional"));
            if (loginSession.canRead(grupId, namamenu)) {                
                provinces = administrasiSession.getProvinsis();
                provinces.add(0, new Pemda("00", "00", "000000", "Nasional", "Nasional", (short)0));
                years = SIKDUtil.getYears((short) 2010);
            }
        } catch (Exception ex) {
            System.out.println("Error bro:" + ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Kegagalan",
                    "Gagal dalam mengambil data User Group. "
                    + ex.toString()));
        }

    }

    double getNilaiGraph(List<GraphGlobal> ls, String kP) {
        double result = 0;
        if (ls != null && !ls.isEmpty()) {
            boolean find = false;
            int ii = 0;
            while (find == false && ii < ls.size()) {
                GraphGlobal gg = ls.get(ii);
                if (gg.getKodePemda().trim().equals(kP.trim())) {
                    result = gg.getNilai();
                    find = true;
                }
                ii++;
            }
        }
        return result;
    }
    
    public final void setData(List<NilaiFungsiPemda> ls){
        barModel = new HorizontalBarChartModel();
        fungsis.clear();
        barModel.setExtender("ext");
        barModel.setAnimate(true);
        
        chartSeries = new ChartSeries();
        double nilai = 1;
        chartSeries.set(" ", 0);
        barHeight = "400px;";
        if(ls!=null && !ls.isEmpty()){
            chartSeries = new ChartSeries();
            if(20*ls.size() > 400)
                barHeight = String.valueOf(20 * ls.size()) + "px;";
            for (NilaiFungsiPemda l : ls) {
                fungsis.add(new NilaiFungsiPemdaObj(l.getKodePemda(), l.getNamaPemda().trim().toUpperCase(), l.getNilai()));
                chartSeries.set(l.getNamaPemda().trim().toUpperCase(), l.getNilai());                
            }
            
        }
        
        barModel.addSeries(chartSeries);
        Axis yAxis = barModel.getAxis(AxisType.Y);
        yAxis.setMin(0);
        yAxis.setMax(nilai);
        yAxis.setTickAngle(-25);

        Axis xAxis = barModel.getAxis(AxisType.X);
        xAxis.setTickAngle(-45);
        xAxis.setTickCount(6);
        barModel.addSeries(chartSeries);
    }

    public HorizontalBarChartModel getBarModel() {
        return barModel;
    }

    public String getSelectedYear() {
        return selectedYear;
    }

    public void setSelectedYear(String selectedYear) {
        this.selectedYear = selectedYear;
    }

    public void onYearChange() {
        labelYear = ""; pemdaLabel="";
        if (selectedYear != null && !selectedYear.trim().equals("")) {
            labelYear = "TAHUN ANGGARAN " + selectedYear;
            if (selectedProvince != null) {                
                try {
                    pemdaLabel = selectedProvince.getNamaPemda().trim().toUpperCase();
                    List<Pemda> kabs;
                    if( selectedProvince.getKodeProvinsi().trim().equals("00"))
                        kabs = administrasiSession.getPemdas();
                    else
                        kabs = administrasiSession.getPemdaByProvinsi(selectedProvince);
                    kabs.add(selectedProvince);
                    List<String> kodes = new ArrayList();
                    
                    for (Pemda kab : kabs) {
                        kodes.add(kab.getKodeProvinsi().trim() + "." + kab.getKodePemda().trim());
                    }
                    List<NilaiFungsiPemda> gg = graphSession.getKompilasiNilaiFungsiPemda(Short.valueOf(selectedYear), kodes, (short)0, (short)1, "10");
                    setData(gg);
                } catch (Exception ex) {
                    Logger.getLogger(KesehatanMBean.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            else setData(null);
        }
        else setData(null);

    }

    public List<String> getYears() {
        return years;
    }

    public List<Pemda> getProvinces() {
        return provinces;
    }

    public Pemda getSelectedProvince() {
        if( selectedProvince!= null && !selectedProvince.getNamaPemda().equals("")) selectedProvince.getNamaPemda().trim().toUpperCase();                
        return selectedProvince;
    }

    public void setSelectedProvince(Pemda selectedProvince) {
        this.selectedProvince = selectedProvince;
    }

    public List<SelectItem> getSelectItemProvinces() {
        return selectItemProvinces;
    }

    public String getLabelYear() {
        return labelYear;
    }

    public void setLabelYear(String labelYear) {
        this.labelYear = labelYear;
    }

    public String getBarHeight() {
        return barHeight;
    }

    public void setBarHeight(String barHeight) {
        this.barHeight = barHeight;
    }

    public List<NilaiFungsiPemdaObj> getFungsis() {
        return fungsis;
    }

    public void setFungsis(List<NilaiFungsiPemdaObj> fungsis) {
        this.fungsis = fungsis;
    }

    public String getPemdaLabel() {
        return pemdaLabel;
    }

    public void setPemdaLabel(String pemdaLabel) {
        this.pemdaLabel = pemdaLabel;
    }
    
}