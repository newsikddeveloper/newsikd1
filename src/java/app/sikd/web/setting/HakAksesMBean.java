package app.sikd.web.setting;

import app.sikd.aas.ServerUtil;
import app.sikd.aas.SessionUtil;
import app.sikd.entity.mgr.TGroupRight;
import app.sikd.entity.mgr.TUserGroup;
import app.sikd.login.ejb.session.AdministrasiSessionBeanRemote;
import app.sikd.login.ejb.session.NewLoginSessionBeanRemote;
import app.sikd.util.SIKDUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.naming.Context;
import javax.servlet.http.HttpServletRequest;
import org.primefaces.event.NodeCollapseEvent;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

/**
 *
 * @author donfebi
 */
@ManagedBean(name = "hakAksesMBean")
@SessionScoped
public class HakAksesMBean implements Serializable {

    private TreeNode root;
    private TreeNode selectedNode;
    private List<TGroupRight> groupRights; 
    private TUserGroup selectedgroup; 

    AdministrasiSessionBeanRemote administrasiSession;
    NewLoginSessionBeanRemote loginSession;
    long grupId;
    boolean writeAccess;

    @PostConstruct
    void init() {
        try {
            String username = SessionUtil.getCookieUsername();
            FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext excontext = context.getExternalContext();
        HttpServletRequest httpServletRequest = (HttpServletRequest)excontext.getRequest();
        String path = httpServletRequest.getRequestURI();
            String namamenu = SIKDUtil.getNamaMenu(path);
            if (username == null) {
                throw new Exception("Identitas pengguna tidak bisa ditemukan atau sudah lewat masa sesi koneksi. "
                        + "Pengguna harus kembali melakukan login.");
            }
            String grupids = SessionUtil.getCookieGroupid();
            if (grupids != null) {
                grupId = Long.valueOf(grupids);
            }

            Context ctxLogin = ServerUtil.getLoginContext();
            administrasiSession = (AdministrasiSessionBeanRemote) ctxLogin.lookup("AdministrasiSessionBean/remote");
            loginSession = (NewLoginSessionBeanRemote) ctxLogin.lookup("NewLoginSessionBean/remote");
            writeAccess = loginSession.canWrite(grupId, namamenu);
            root = new DefaultTreeNode("root", null);
            
            if (loginSession.canRead(grupId, namamenu)) {
                TUserGroup tug = administrasiSession.getUserGroup(grupId);
                TreeNode accnode = new DefaultTreeNode(tug, root);
                List<TUserGroup> subs = administrasiSession.getSubUserGroups(tug);
                tug.setSubGroups(subs);
                for (int i = 0; i < subs.size(); i++) {
                    TUserGroup get = subs.get(i);
                    TreeNode aa = new DefaultTreeNode(get, accnode);
                }
                groupRights = new ArrayList();
            }
        } catch (Exception ex) {
            System.out.println("Error bro:" + ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Kegagalan",
                    "Gagal dalam mengambil data User Group. "
                    + ex.toString()));
        }
    }

    public TreeNode getRoot() {
        return root;
    }

    public void setRoot(TreeNode root) {
        this.root = root;
    }

    public TreeNode getSelectedNode() {
        return selectedNode;
    }

    public void setSelectedNode(TreeNode selectedNode) {
        this.selectedNode = selectedNode;
    }


    public void onNodeSelected(NodeSelectEvent event) {
        TreeNode node = event.getTreeNode();
        selectedgroup = (TUserGroup) node.getData();

        if (selectedgroup.getSubGroups()==null) {
            try {
                List<TUserGroup> subs = administrasiSession.getSubUserGroups(selectedgroup);
                selectedgroup.setSubGroups(subs);

                for (TUserGroup su : subs) {
                        TreeNode accnode = new DefaultTreeNode(su, node);
                }
            } catch (Exception ex) {
                System.out.println("Error bro:" + ex);
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        "Kegagalan",
                        "Gagal dalam mengambil data sub group. "
                        + ex.toString()));
            }
        }
        
        try {
            groupRights = administrasiSession.getGroupRight(selectedgroup.getId());
        } catch (Exception ex) {
            Logger.getLogger(HakAksesMBean.class.getName()).log(Level.SEVERE, null, ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        "Kegagalan",
                        "Gagal dalam mengambil data Hak Akses Group. "
                        + ex.toString()));
        }
    }

    public void onNodeExpanded(NodeExpandEvent event) {
        System.out.println("onNodeExpanded");
    }

    public void onNodeCollapsed(NodeCollapseEvent event) {
        System.out.println("onNodeCollapsed");
    }

    public boolean isWriteAccess() {
        return writeAccess;
    }

    public void setWriteAccess(boolean writeAccess) {
        this.writeAccess = writeAccess;
    }

    public List<TGroupRight> getGroupRights() {
        return groupRights;
    }

    public void setGroupRights(List<TGroupRight> groupRights) {
        this.groupRights = groupRights;    
    }
    
    public void onSave(){
        if(selectedNode != null){
            TUserGroup userGroup = (TUserGroup)selectedNode.getData();
            try {
                administrasiSession.updateGroupRight(userGroup.getId(), groupRights);
                groupRights = administrasiSession.getGroupRight(userGroup.getId());
            } catch (Exception ex) {
                Logger.getLogger(HakAksesMBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    

    public TUserGroup getSelectedgroup() {
        return selectedgroup;
    }

    public void setSelectedgroup(TUserGroup selectedgroup) {
        this.selectedgroup = selectedgroup;
    }

    public long getGrupId() {
        return grupId;
    }

    public void setGrupId(long grupId) {
        this.grupId = grupId;
    }
    
}