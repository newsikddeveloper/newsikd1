package app.sikd.web.setting;

import app.sikd.aas.ServerUtil;
import app.sikd.aas.SessionUtil;
import app.sikd.entity.Waktu;
import app.sikd.entity.backoffice.StatusPemda;
import app.sikd.entity.backoffice.setting.StatusData;
import app.sikd.login.ejb.session.NewLoginSessionBeanRemote;
import app.sikd.office.ejb.newsession.NewSettingSessionBeanRemote;
import app.sikd.util.SIKDUtil;
import app.sikd.web.statusdata.UpdateStatusDataMBean;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.naming.Context;
import javax.servlet.http.HttpServletRequest;
import org.primefaces.event.RowEditEvent;

/**
 *
 * @author soraturaes
 */
@ManagedBean
@ViewScoped
public class WaktuMBean implements Serializable {
    List<Waktu> waktus;
    Waktu selectedWaktu;
    NewSettingSessionBeanRemote settingSession;
    NewLoginSessionBeanRemote loginSession;
    long grupId;
    boolean writeAccess;

    @PostConstruct
    void init() {
        try {
            String username = SessionUtil.getCookieUsername();
            FacesContext context = FacesContext.getCurrentInstance();
            ExternalContext excontext = context.getExternalContext();
            HttpServletRequest httpServletRequest = (HttpServletRequest)excontext.getRequest();
            String path = httpServletRequest.getRequestURI();
            String namamenu = SIKDUtil.getNamaMenu(path);
                        
            if (username == null) {
                throw new Exception("Identitas pengguna tidak bisa ditemukan atau sudah lewat masa sesi koneksi. "
                        + "Pengguna harus kembali melakukan login.");
            }
            String grupids = SessionUtil.getCookieGroupid();
            if (grupids != null) {
                grupId = Long.valueOf(grupids);
            }

            Context ctxLogin = ServerUtil.getLoginContext();
            settingSession = (NewSettingSessionBeanRemote) ctxLogin.lookup("NewSettingSessionBean/remote");
            loginSession = (NewLoginSessionBeanRemote) ctxLogin.lookup("NewLoginSessionBean/remote");
            writeAccess = loginSession.canWrite(grupId, namamenu);
            waktus = new ArrayList();
            if (loginSession.canRead(grupId, namamenu)) {
                waktus = settingSession.getWaktus();
            }
        } catch (Exception ex) {
            System.out.println("Error bro:" + ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Kegagalan",
                    "Gagal dalam mengambil data Setting Waktu. "
                    + ex.toString()));
        }
    }

    public List<Waktu> getWaktus() {
        return waktus;
    }

    public void setWaktus(List<Waktu> waktus) {
        this.waktus = waktus;
    }

    public Waktu getSelectedWaktu() {
        return selectedWaktu;
    }

    public void setSelectedWaktu(Waktu selectedWaktu) {
        this.selectedWaktu = selectedWaktu;
    }
    
    public void onEdit(RowEditEvent event) {
        try {
            if( event.getObject() instanceof Waktu){
                Waktu w = (Waktu) event.getObject();
                if(w!=null && w.getIkd()!=null){
                    settingSession.updateWaktu(w);
                }
            }
    } catch (Exception ex) {
        Logger.getLogger(WaktuMBean.class.getName()).log(Level.SEVERE, null, ex);
    } 
}
    


}