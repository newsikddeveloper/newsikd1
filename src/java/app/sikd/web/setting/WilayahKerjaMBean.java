package app.sikd.web.setting;

import app.sikd.aas.ServerUtil;
import app.sikd.aas.SessionUtil;
import app.sikd.entity.Pemda;
import app.sikd.entity.WilayahKerja;
import app.sikd.login.ejb.session.AdministrasiSessionBeanRemote;
import app.sikd.login.ejb.session.NewLoginSessionBeanRemote;
import app.sikd.util.SIKDUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.naming.Context;
import javax.servlet.http.HttpServletRequest;
import org.primefaces.event.NodeCollapseEvent;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

/**
 *
 * @author donfebi
 */
@ManagedBean(name = "wilayahKerjaMBean")
@ViewScoped
public class WilayahKerjaMBean implements Serializable {

    private List<WilayahKerja> wilayahs;
    private List<Pemda> allPemdas;
    private List<PemdaSelectObj> pemdaObjs;
    private WilayahKerja selectedWilayah;
    private String selectedNamaWilayah;

    AdministrasiSessionBeanRemote administrasiSession;
    NewLoginSessionBeanRemote loginSession;
    long grupId;
    boolean writeAccess;

    TreeNode root;
    TreeNode pemdaRoot;
    TreeNode addpemdaRoot;
    TreeNode selectedNode;
    boolean edit;
    boolean hapus;
    int mode=0;

    @PostConstruct
    void init() {
        try {
            String username = SessionUtil.getCookieUsername();
            FacesContext context = FacesContext.getCurrentInstance();
            ExternalContext excontext = context.getExternalContext();
            HttpServletRequest httpServletRequest = (HttpServletRequest) excontext.getRequest();
            String path = httpServletRequest.getRequestURI();
            String namamenu = SIKDUtil.getNamaMenu(path);
            if (username == null) {
                throw new Exception("Identitas pengguna tidak bisa ditemukan atau sudah lewat masa sesi koneksi. "
                        + "Pengguna harus kembali melakukan login.");
            }
            String grupids = SessionUtil.getCookieGroupid();
            if (grupids != null) {
                grupId = Long.valueOf(grupids);
            }

            Context ctxLogin = ServerUtil.getLoginContext();
            administrasiSession = (AdministrasiSessionBeanRemote) ctxLogin.lookup("AdministrasiSessionBean/remote");
            loginSession = (NewLoginSessionBeanRemote) ctxLogin.lookup("NewLoginSessionBean/remote");
            writeAccess = loginSession.canWrite(grupId, namamenu);
            root = new DefaultTreeNode("root", null);
            pemdaRoot = new DefaultTreeNode("root", null);
            addpemdaRoot = new DefaultTreeNode("root", null);
            selectedNamaWilayah="";
            selectedNode=null;
            selectedWilayah=null;

            if (loginSession.canRead(grupId, namamenu)) {
                wilayahs = administrasiSession.getWilayahKerjas();
                for (int i = 0; i < wilayahs.size(); i++) {
                    WilayahKerja get = wilayahs.get(i);
                    TreeNode accnode = new DefaultTreeNode(get, root);
                }
            }
            allPemdas = administrasiSession.getPemdas();
            addpemdaRoot = new DefaultTreeNode("root", null);
            if (allPemdas != null) {
                for (int i = 0; i < allPemdas.size(); i++) {
                    Pemda get = allPemdas.get(i);
                    TreeNode tn = new DefaultTreeNode(new PemdaSelectObj(get, false), addpemdaRoot);
                }
            }
        } catch (Exception ex) {
            System.out.println("Error bro:" + ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Kegagalan",
                    "Gagal dalam mengambil data Wilayah Kerja. "
                    + ex.toString()));
        }
    }

    public TreeNode getRoot() {
        return root;
    }

    public void setRoot(TreeNode root) {
        this.root = root;
    }

    public TreeNode getSelectedNode() {
        return selectedNode;
    }

    public void setSelectedNode(TreeNode selectedNode) {
        this.selectedNode = selectedNode;
    }
    
    public void onAdd(){
        selectedNode = new DefaultTreeNode("",null);
        selectedWilayah = new WilayahKerja();
        List<TreeNode> childs = addpemdaRoot.getChildren();
        for (int i = 0; i < childs.size(); i++) {
            ((PemdaSelectObj)childs.get(i).getData()).setSelected(false);
            
        }
        mode=1;
    }
    
    public void onEdit(){
        List<TreeNode> childs = addpemdaRoot.getChildren();
        for (int i = 0; i < childs.size(); i++) {
            boolean cek = findPemdaSelect(selectedWilayah.getPemdas(), ((PemdaSelectObj)childs.get(i).getData()).getPemda().getId());
            ((PemdaSelectObj)childs.get(i).getData()).setSelected(cek);
            
        }
        mode=2;
    }
    
    boolean findPemdaSelect(List<Pemda> ps, long idPemda){
        boolean find = false;
        int ii = 0;
        while(find==false && ii<ps.size()){
            if(ps.get(ii).getId() == idPemda){
                find = true;
            }
            ii++;
        }
        return find;
    }

    public void onNodeSelected(NodeSelectEvent event) {
        pemdaRoot = new DefaultTreeNode("root", null);
        TreeNode node = event.getTreeNode();
        selectedWilayah = (WilayahKerja) node.getData();
        edit = true;
        hapus = true;
        if (selectedWilayah.getPemdas() != null) {
            for (int i = 0; i < selectedWilayah.getPemdas().size(); i++) {
                Pemda get = selectedWilayah.getPemdas().get(i);
                TreeNode pemdanode = new DefaultTreeNode(get, pemdaRoot);
            }
        }
    }

    public void onNodeExpanded(NodeExpandEvent event) {
        System.out.println("onNodeExpanded");
    }

    public void onNodeCollapsed(NodeCollapseEvent event) {
        System.out.println("onNodeCollapsed");
    }

    public boolean isWriteAccess() {
        return writeAccess;
    }

    public void setWriteAccess(boolean writeAccess) {
        this.writeAccess = writeAccess;
    }

    public TreeNode getPemdaRoot() {
        return pemdaRoot;
    }

    public void setPemdaRoot(TreeNode pemdaRoot) {
        this.pemdaRoot = pemdaRoot;
    }
    
    WilayahKerja getNewData () throws Exception{
        if(selectedWilayah.getNama()==null || selectedWilayah.getNama().trim().equals(""))
            throw new Exception("Silahakn isi Nama Wilayah Kerja");
        
        List<TreeNode> childs = addpemdaRoot.getChildren();
        List<Pemda> dataPemdas = new ArrayList();
        for (int i = 0; i < childs.size(); i++) {
            if(((PemdaSelectObj)childs.get(i).getData()).isSelected()){
                dataPemdas.add(((PemdaSelectObj)childs.get(i).getData()).getPemda());
            }
        }
        WilayahKerja wk = new WilayahKerja(selectedWilayah.getNama(), "");        
        wk.setPemdas(dataPemdas);
        if(mode==2) wk.setId(selectedWilayah.getId());
        return wk;
    }
    
    public void onSave(){
        try {
            WilayahKerja wk = getNewData();
            if (mode == 1) {
                wk = administrasiSession.createWilayahKerja(wk);
                TreeNode tr = new DefaultTreeNode(wk, root);                
            }
            else if (mode == 2){
                administrasiSession.updateWilayahKerja(wk);
                init();
            }
            mode=0;
        } catch (Exception ex) {
            Logger.getLogger(WilayahKerjaMBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void onDelete(){
        try {
            administrasiSession.deleteWilayahKerja(selectedWilayah);
            init();
            edit = true;
            hapus = true;
        } catch (Exception ex) {
            Logger.getLogger(WilayahKerjaMBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public long getGrupId() {
        return grupId;
    }

    public void setGrupId(long grupId) {
        this.grupId = grupId;
    }

    public boolean isEdit() {
        return edit;
    }

    public void setEdit(boolean edit) {
        this.edit = edit;
    }

    public boolean isHapus() {
        return hapus;
    }

    public void setHapus(boolean hapus) {
        this.hapus = hapus;
    }

    public WilayahKerja getSelectedWilayah() {
        return selectedWilayah;
    }

    public void setSelectedWilayah(WilayahKerja selectedWilayah) {
        this.selectedWilayah = selectedWilayah;
    }

    public TreeNode getAddpemdaRoot() {
        return addpemdaRoot;
    }

    public void setAddpemdaRoot(TreeNode addpemdaRoot) {
        this.addpemdaRoot = addpemdaRoot;
    }
    
    

}
