/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.setting;

import app.sikd.aas.ServerUtil;
import app.sikd.aas.SessionUtil;
import app.sikd.entity.mgr.TUserGroup;
import app.sikd.login.ejb.session.AdministrasiSessionBeanRemote;
import app.sikd.login.ejb.session.NewLoginSessionBeanRemote;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.naming.Context;
import org.primefaces.event.NodeCollapseEvent;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

@ManagedBean(name = "groupBean2")
@ViewScoped
public class GroupMBean2 implements Serializable{
    TUserGroup selectedGroup;
    TreeNode selectedNode;
    
    AdministrasiSessionBeanRemote administrasiSession;
    NewLoginSessionBeanRemote loginSession;
    boolean iswrites=true;
    long grupId = 0;
    
    private TreeNode root;
    
    @PostConstruct
    public void init(){
        try {
            System.out.println("init group");
            Context ctxLogin = ServerUtil.getLoginContext();
            loginSession = (NewLoginSessionBeanRemote) ctxLogin.lookup("NewLoginSessionBean/remote");
            administrasiSession = (AdministrasiSessionBeanRemote) ctxLogin.lookup("AdministrasiSessionBean/remote");
            String grupids = SessionUtil.getCookieGroupid();
            if(grupids != null) grupId = Long.valueOf(grupids);
            initData();
//            iswrites = loginSession.canWrite(idgrup, null)
            
        } catch (Exception ex) {
            Logger.getLogger(GroupBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    void initData(){
        try {
            
            TUserGroup grup = administrasiSession.getUserGroup(grupId);
            List<TUserGroup> grups = administrasiSession.getSubUserGroups(grup);
            grup.setSubGroups(grups);
            root = new DefaultTreeNode("", null);
            TreeNode droot = new DefaultTreeNode(grup, root);
            if(grups!= null ){
                TreeNode node0;
                for (int i = 0; i < grups.size(); i++) {
                    TUserGroup get = grups.get(i);
                    node0 = new DefaultTreeNode(get, droot);
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(GroupBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void onNodeSelect(NodeSelectEvent event) {
        Object node = event.getTreeNode().getData();
        System.out.println("pilij "+ node);
        if(node instanceof TUserGroup ){
            
            selectedGroup = (TUserGroup) node;
            System.out.println("selected "+selectedGroup );
            if(selectedGroup.getSubGroups()==null || selectedGroup.getSubGroups().isEmpty()){
                try {
                    List<TUserGroup> grups = administrasiSession.getSubUserGroups(selectedGroup);
                    if(grups != null ){
                        for (int i = 0; i < grups.size(); i++) {
                            TUserGroup get = grups.get(i);
                            TreeNode tn = new DefaultTreeNode(get, event.getTreeNode());
                        }
                        selectedGroup.setSubGroups(grups);
                    }
                } catch (Exception ex) {
                    Logger.getLogger(GroupBean.class.getName()).log(Level.SEVERE, null, ex);
                }
                
            }
        }
    }

    public void onNodeExpand(NodeExpandEvent event) {
        String node = event.getTreeNode().getData().toString();
    }

    public void onNodeCollapse(NodeCollapseEvent event) {
        String node = event.getTreeNode().getData().toString();
    }

    public TUserGroup getSelectedGroup() {
        return selectedGroup;
    }

    public void setSelectedGroup(TUserGroup selectedGroup) {
        this.selectedGroup = selectedGroup;
    }

    public boolean isIswrites() {
        return iswrites;
    }

    public void setIswrites(boolean iswrites) {
        this.iswrites = iswrites;
    }

    public TreeNode getRoot() {
        return root;
    }

    public void setRoot(TreeNode root) {
        this.root = root;
    }

    public TreeNode getSelectedNode() {
        return selectedNode;
    }

    public void setSelectedNode(TreeNode selectedNode) {
        this.selectedNode = selectedNode;
    }
    
}
