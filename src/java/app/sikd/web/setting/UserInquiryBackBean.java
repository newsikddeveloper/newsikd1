/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.web.setting;

import app.sikd.aas.ServerUtil;
import app.sikd.entity.mgr.TUserAccount;
import app.sikd.login.ejb.session.AdministrasiSessionBeanRemote;
import app.sikd.util.SIKDUtil;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.naming.Context;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Dell
 */

@ManagedBean(name="userInquiry") 
@ViewScoped 
public class UserInquiryBackBean extends AInquiryBackBean<TUserAccount> {

    private AdministrasiSessionBeanRemote service;
    private List<TUserAccount> datas;
    private String userName;
    private TUserAccount selectedUserAccount;

    @PostConstruct
    protected void init() {
        try {
            Context context = ServerUtil.getOfficeContext();
            service = (AdministrasiSessionBeanRemote) context.lookup("AdministrasiSessionBean/remote");
            
            FacesContext fc = FacesContext.getCurrentInstance();
            ExternalContext excontext = fc.getExternalContext();
            HttpServletRequest req = (HttpServletRequest)excontext.getRequest();
            TUserAccount uac = (TUserAccount)req.getSession().getAttribute(SIKDUtil.SESSION_USE);
            userName = uac.getUsername();
            
            loadData();
            
        } catch (Exception ex) {
            Logger.getLogger(UserInquiryBackBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public void loadData() throws Exception {
        datas = service.getInquiryUser(userName);
    }

    @Override
    public List<TUserAccount> getData() {
        return datas;
    }

    @Override
    public String detailPage() {
        return "/Pengaturan/AdministrasiPengguna/Pengguna/UserDataInput.jsf";
    }

    @Override
    public void doAdd() {
        callPage();
    }

    private void callPage() {
       try {
        String dom = ServerUtil.getDomain();
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext excontext = facesContext.getExternalContext();
        excontext.redirect(dom + excontext.getRequestContextPath() + detailPage());
         
        } catch (IOException ex) {
            Logger.getLogger(AInquiryBackBean.class.getName()).log(Level.SEVERE, null, ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Error: ", ex.getMessage()));
        }
    }
    
    public TUserAccount getSelectedUserAccount() {
        return selectedUserAccount;
    }

    public void setSelectedUserAccount(TUserAccount selectedUserAccount) {
        this.selectedUserAccount = selectedUserAccount;
    }
    
    
}
