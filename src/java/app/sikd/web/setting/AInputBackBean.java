/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.web.setting;

import app.sikd.web.common.EditorState;
import java.io.Serializable;

/**
 *
 * @author Dell
 */
public abstract class AInputBackBean<DATA> implements Serializable {
    protected EditorState editorState;
    public abstract DATA getEditingData();
    protected abstract String mainPage();
    protected abstract DATA instantiateNewData();
    protected abstract void maintainNewData()throws Exception;
    public abstract void doSave();
    public abstract void doCancel();
}
