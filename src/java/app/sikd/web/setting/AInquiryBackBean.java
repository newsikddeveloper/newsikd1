/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.web.setting;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Dell
 */
public abstract class AInquiryBackBean<DATA> implements Serializable{
    public abstract void loadData() throws Exception;
    public abstract List<DATA> getData() throws Exception;
    public abstract String detailPage();
    public abstract void doAdd();    
}
