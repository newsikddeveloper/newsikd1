/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.setting;

import app.sikd.aas.ServerUtil;
import app.sikd.entity.Pemda;
import app.sikd.entity.mgr.TUserAccount;
import app.sikd.entity.mgr.TUserContact;
import app.sikd.entity.mgr.TUserGroup;
import app.sikd.login.ejb.session.AdministrasiSessionBeanRemote;
import app.sikd.web.common.EditorState;
import app.sikd.web.common.bean.IPemdaFilteredBean;
import app.sikd.web.common.bean.IUserGroupFilteredBean;
import app.sikd.web.common.filter.PemdaFilter;
import app.sikd.web.common.filter.UserGroupFilter;
import app.sikd.web.common.listener.IFilterChangedListener;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.naming.Context;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author Dell
 */
@ManagedBean(name = "userDataInput")
@ViewScoped
public class UserDataInputBackBean extends AInputBackBean<TUserAccount> implements IPemdaFilteredBean, IUserGroupFilteredBean, IFilterChangedListener {

    private AdministrasiSessionBeanRemote service;
    private PemdaFilter pemdaFilter;
    private UserGroupFilter groupFilter;
    private Pemda pemda;
    private TUserGroup userGroup;
    private TUserAccount userAccount;
    private String password;
    private TUserContact userContact;
    private UploadedFile file;

    @PostConstruct
    protected void init() {
        try {
            Context context = ServerUtil.getOfficeContext();
            service = (AdministrasiSessionBeanRemote) context.lookup("AdministrasiSessionBean/remote");

            pemdaFilter = new PemdaFilter();
            groupFilter = new UserGroupFilter();
            userContact = new TUserContact();
            userAccount = new TUserAccount();
            editorState = EditorState.add;

        } catch (Exception ex) {
            Logger.getLogger(UserDataInputBackBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<Pemda> getPemdas() {
        return pemdaFilter.getPemdas();
    }

    @Override
    public void setSelectedPemda(Pemda pemda) {
        this.pemda = pemda;
    }

    @Override
    public Pemda getSelectedPemda() {
        return this.pemda;
    }

    @Override
    public List<TUserGroup> getUserGroups() {
        return groupFilter.getUserGroups();
    }

    @Override
    public void setSelectedUserGroup(TUserGroup userGroup) {
        this.userGroup = userGroup;
    }

    @Override
    public TUserGroup getSelectedUserGroup() {
        return this.userGroup;
    }

    @Override
    public void onFilterChanged() {

    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public TUserContact getUserContact() {
        return userContact;
    }

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    @Override
    protected String mainPage() {
        return "/Pengaturan/AdministrasiPengguna/Pengguna.jsf";
    }

    @Override
    protected TUserAccount instantiateNewData() {
        return new TUserAccount();
    }

    @Override
    protected void maintainNewData() throws Exception {
        if (userAccount.getUsername() == null || userAccount.getUsername().trim().equals("")) {
            throw new Exception("Silahkan isi data Username");
        }
        if (password == null || password.trim().equals("")) {
            throw new Exception("Silahkan isi Password");
        }
        if (userGroup == null) {
            throw new Exception("Silahkan isi Grup User");
        }

        byte[] pas = new byte[password.trim().length()];
        for (int i = 0; i < pas.length; i++) {
            pas[i] = (byte) password.charAt(i);
        }

        userAccount.setPassword(pas);
        userAccount.setActives(true);
        userAccount.setGroup(userGroup);
        userAccount.setUserContact(userContact);
        
//         try {
//            File targetFolder = new File("/var/uploaded/images");
//            InputStream inputStream = file.getInputstream();
//            OutputStream out = new FileOutputStream(new File(targetFolder,
//                    file.getFile().getFileName()));
//            int read = 0;
//            byte[] bytes = new byte[1024];
//            while ((read = inputStream.read(bytes)) != -1) {
//                out.write(bytes, 0, read);
//            }
//            inputStream.close();
//            out.flush();
//            out.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

    }

    @Override
    public void doSave() {
        try {
            if (editorState.toString().equals(EditorState.add.toString())) {
                maintainNewData();
                service.createUser(userAccount);
            } else {
            }
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Kegagalan", ex.getMessage()));
            Logger.getLogger(UserDataInputBackBean.class.getName()).log(Level.SEVERE, null, ex);
            return;
        }
        callPage();
    }

    @Override
    public void doCancel() {
        callPage();
    }

    private void callPage() {
        try {
            String dom = ServerUtil.getDomain();
            FacesContext facesContext = FacesContext.getCurrentInstance();
            ExternalContext excontext = facesContext.getExternalContext();
            excontext.redirect(dom + excontext.getRequestContextPath() + mainPage());

        } catch (IOException ex) {
            Logger.getLogger(AInquiryBackBean.class.getName()).log(Level.SEVERE, null, ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Error: ", ex.getMessage()));
        }
    }

    @Override
    public TUserAccount getEditingData() {
        return this.userAccount;
    }

    public void handleFileUpload(FileUploadEvent event) {
        System.out.println("upload");
        // Get uploaded file from the FileUploadEvent
//        this.file = event.getFile();
        // Print out the information of the file
//        System.out.println("Uploaded File Name Is :: " + file.getFileName() + " :: Uploaded File Size :: " + file.getSize());
    }
    public void handleFileUploadChange() {
        System.out.println("rubah");
    }
}
