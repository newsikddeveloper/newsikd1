/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.setting;

import app.sikd.entity.Pemda;
import java.io.Serializable;

/**
 *
 * @author sora
 */
public class PemdaSelectObj implements Serializable{
    Pemda pemda;
    boolean selected;

    public PemdaSelectObj() {
    }

    public PemdaSelectObj(Pemda pemda, boolean selected) {
        this.pemda = pemda;
        this.selected = selected;
    }

    public Pemda getPemda() {
        return pemda;
    }

    public void setPemda(Pemda pemda) {
        this.pemda = pemda;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    @Override
    public String toString() {
        return pemda.toString();
    }
    
}
