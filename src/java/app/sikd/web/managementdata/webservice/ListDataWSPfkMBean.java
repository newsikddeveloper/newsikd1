package app.sikd.web.managementdata.webservice;

import app.sikd.aas.ServerUtil;
import app.sikd.aas.SessionUtil;
import app.sikd.entity.backoffice.ListIkdPemda;
import app.sikd.login.ejb.session.AdministrasiSessionBeanRemote;
import app.sikd.login.ejb.session.NewLoginSessionBeanRemote;
import app.sikd.office.ejb.newsession.NewManajemenDataSessionBeanRemote;
import app.sikd.util.SIKDUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.naming.Context;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author sora
 */
@ManagedBean
@ViewScoped

public class ListDataWSPfkMBean implements Serializable {
    List<String> years;
    String selectedYear;
    String labelYear;

    AdministrasiSessionBeanRemote administrasiSession;
    NewLoginSessionBeanRemote loginSession;
    NewManajemenDataSessionBeanRemote manajemenSession;
    long grupId;
    
    List<ListIkdPemda> listIkdPemdas;
    String jumlahDaerah="";
    
    @PostConstruct
    public void init() {
        try {
            labelYear = "";
            String username = SessionUtil.getCookieUsername();
            FacesContext context = FacesContext.getCurrentInstance();
            ExternalContext excontext = context.getExternalContext();
            HttpServletRequest httpServletRequest = (HttpServletRequest) excontext.getRequest();
            String path = httpServletRequest.getRequestURI();
            String namamenu = SIKDUtil.getNamaMenu(path);
            if (username == null) {
                throw new Exception("Identitas pengguna tidak bisa ditemukan atau sudah lewat masa sesi koneksi. "
                        + "Pengguna harus kembali melakukan login.");
            }
            String grupids = SessionUtil.getCookieGroupid();
            if (grupids != null) {
                grupId = Long.valueOf(grupids);
            }

            Context ctxLogin = ServerUtil.getLoginContext();
            Context ctxOffice = ServerUtil.getOfficeContext();
            administrasiSession = (AdministrasiSessionBeanRemote) ctxLogin.lookup("AdministrasiSessionBean/remote");
            loginSession = (NewLoginSessionBeanRemote) ctxLogin.lookup("NewLoginSessionBean/remote");
            manajemenSession = (NewManajemenDataSessionBeanRemote) ctxOffice.lookup("NewManajemenDataSessionBean/remote");

            years = new ArrayList();
            listIkdPemdas = new ArrayList();
            if (loginSession.canRead(grupId, namamenu)) {
                years = SIKDUtil.getYears((short) 2010);
            }
        } catch (Exception ex) {
            System.out.println("Error bro:" + ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Kegagalan",
                    "Gagal dalam mengambil data User Group. "
                    + ex.toString()));
        }

    }

    public String getSelectedYear() {
        return selectedYear;
    }

    public void setSelectedYear(String selectedYear) {
        this.selectedYear = selectedYear;
    }

    public void onYearChange() {
        labelYear = ""; 
        listIkdPemdas.clear();
        if (selectedYear != null && !selectedYear.trim().equals("")) {
            try {
                labelYear = "TAHUN ANGGARAN " + selectedYear;
                listIkdPemdas=manajemenSession.getListDataPfkSending(Short.valueOf(selectedYear), (short)1);
                jumlahDaerah = "Jumlah Daerah pengirim Perhitungan Fihak Ketiga menggunakan WebService : " + listIkdPemdas.size();
            } catch (Exception ex) {
                Logger.getLogger(ListDataWSPfkMBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public List<String> getYears() {
        return years;
    }

    public String getLabelYear() {
        return labelYear;
    }

    public void setLabelYear(String labelYear) {
        this.labelYear = labelYear;
    }

    public List<ListIkdPemda> getListIkdPemdas() {
        return listIkdPemdas;
    }
    
    public String getJumlahDaerah() {
        return jumlahDaerah;
    }
    
}
