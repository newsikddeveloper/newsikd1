package app.sikd.web.managementdata.webservice;

import app.sikd.aas.ServerUtil;
import app.sikd.entity.backoffice.ListIkdPemda;
import app.sikd.login.ejb.session.NewLoginSessionBeanRemote;
import app.sikd.office.ejb.newsession.NewManajemenDataSessionBeanRemote;
import app.sikd.office.ejb.session.APBDSessionBeanRemote;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;

/**
 *
 * @author sora
 */
public class ListDataWsApbdReportWorker {
    private APBDSessionBeanRemote service;
    private NewLoginSessionBeanRemote loginService;
    private NewManajemenDataSessionBeanRemote manajemenService;

    public ListDataWsApbdReportWorker() {
        try {
            Context loginContext = ServerUtil.getLoginContext();
            loginService = (NewLoginSessionBeanRemote) loginContext.lookup("NewLoginSessionBean/remote");

            Context context = ServerUtil.getOfficeContext();
            service = (APBDSessionBeanRemote) context.lookup("APBDSessionBean/remote");
            manajemenService = (NewManajemenDataSessionBeanRemote) context.lookup("NewManajemenDataSessionBean/remote");

        } catch (Exception ex) {
            Logger.getLogger(ListDataWsApbdReportWorker.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    
    public List<ListIkdPemda> updateDataReport(short _year, short _kodeData, short _jenisCoa) throws Exception {
        List<ListIkdPemda> ls = manajemenService.getListDataApbdSending(_year, _kodeData, _jenisCoa, (short)0);
        return ls;
    }

}
