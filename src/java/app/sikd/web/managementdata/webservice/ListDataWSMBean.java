package app.sikd.web.managementdata.webservice;

import app.sikd.aas.ServerUtil;
import app.sikd.aas.SessionUtil;
import app.sikd.util.SIKDUtil;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Nongky
 */
@ManagedBean
@ViewScoped
public class ListDataWSMBean implements Serializable{
    long grupId;
    String apbdPage;
    String lraPage;
    @PostConstruct
    private void init() {
        try {
            String username = SessionUtil.getCookieUsername();
            FacesContext context = FacesContext.getCurrentInstance();
            ExternalContext excontext = context.getExternalContext();
            HttpServletRequest httpServletRequest = (HttpServletRequest) excontext.getRequest();
            String path = httpServletRequest.getRequestURI();
            String namamenu = SIKDUtil.getNamaMenu(path);
            
            if (username == null) {
                throw new Exception("Identitas pengguna tidak bisa ditemukan atau sudah lewat masa sesi koneksi. "
                        + "Pengguna harus kembali melakukan login.");
            }
            
            String grupids = SessionUtil.getCookieGroupid();
            if (grupids != null) {
                grupId = Long.valueOf(grupids);
            }

            String dom = ServerUtil.getDomain();
//            excontext.redirect(dom + excontext.getRequestContextPath() + "/home.jsf");
//            if (loginSession.canRead(grupId, namamenu)) {
//                provinces = administrasiSession.getProvinsis();
//                provinces.add(0, new Pemda("00", "00", "000000", "Nasional", "Nasional", (short) -1));
//                years = SIKDUtil.getYears((short) 2010);
//            }
//            setPage(dom+excontext.getRequestContextPath()+"/ManajemenData/WebService/apbd/listdataws_apbd_index.xhtml");
            apbdPage = dom+excontext.getRequestContextPath()+"/ManajemenData/WebService/ListDataApbdWS.xhtml";
            apbdPage = dom+excontext.getRequestContextPath()+"/ManajemenData/WebService/ListDataLraWS.xhtml";
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getApbdPage() {
        return apbdPage;
    }

    public String getLraPage() {
        return lraPage;
    }
    
}
