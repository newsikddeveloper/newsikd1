package app.sikd.web.managementdata.xml;

import app.sikd.aas.ServerUtil;
import app.sikd.aas.SessionUtil;
import app.sikd.entity.Pemda;
import app.sikd.entity.WilayahKerja;
import app.sikd.entity.mgr.TUserAccount;
import app.sikd.entity.utilitas.UploadFile;
import app.sikd.entity.ws.Apbd_WS;
import app.sikd.entity.ws.PerhitunganFihakKetiga_WS;
import app.sikd.entity.ws.fault.SIKDServiceException;
import app.sikd.login.ejb.session.NewLoginSessionBeanRemote;
import app.sikd.service.session.APBDServiceSessionBeanRemote;
import app.sikd.util.SIKDUtil;
import static com.google.common.io.ByteStreams.copy;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.naming.Context;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author PC04
 */
@ManagedBean
@ViewScoped

public class UploadPfkXmlMBean implements Serializable {
    String namaPemda;
    List<String> years;
    String selectedYear;
    String labelYear;
    List<Pemda> pemdas;
    Pemda selectedPemda;
    private List<String> stats;
    private UploadFile selectedUploadFile;

//    AdministrasiSessionBeanRemote administrasiSession;
//    APBDSessionBeanRemote apbdSession;
    NewLoginSessionBeanRemote loginSession;    
    APBDServiceSessionBeanRemote apbdServiceSession;
    long grupId;

    boolean viewPemda;
    
    PerhitunganFihakKetiga_WS pfk;

    @PostConstruct
    public void init() {
        try {
            labelYear = "";
            String username = SessionUtil.getCookieUsername();
            FacesContext context = FacesContext.getCurrentInstance();
            ExternalContext excontext = context.getExternalContext();
            HttpServletRequest httpServletRequest = (HttpServletRequest) excontext.getRequest();
            String path = httpServletRequest.getRequestURI();
            String namamenu = SIKDUtil.getNamaMenu(path);
            if (username == null) {
                throw new Exception("Identitas pengguna tidak bisa ditemukan atau sudah lewat masa sesi koneksi. "
                        + "Pengguna harus kembali melakukan login.");
            }
            String grupids = SessionUtil.getCookieGroupid();
            if (grupids != null) {
                grupId = Long.valueOf(grupids);
            }

            Context ctxLogin = ServerUtil.getLoginContext();
            Context ctxOffice = ServerUtil.getOfficeContext();
//            administrasiSession = (AdministrasiSessionBeanRemote) ctxLogin.lookup("AdministrasiSessionBean/remote");
            loginSession = (NewLoginSessionBeanRemote) ctxLogin.lookup("NewLoginSessionBean/remote");
            apbdServiceSession = (APBDServiceSessionBeanRemote) ctxLogin.lookup("APBDServiceSessionBean/remote");
            stats = new ArrayList();
            years = new ArrayList();
            pemdas = new ArrayList();
            namaPemda = "";
            if (loginSession.canRead(grupId, namamenu)) {
                years = SIKDUtil.getYears((short) 2010);
                TUserAccount user = loginSession.getUserbyName(username);
                WilayahKerja wilker = user.getWilayah();
                if (wilker != null && wilker.getPemdas() != null) {
                    pemdas = wilker.getPemdas();
                    viewPemda = true;
                } else {
                    viewPemda = false;
                    if (user.getPemda() != null) {
                        selectedPemda = user.getPemda();
                        namaPemda = selectedPemda.getNamaPemda();
                    }
                }
            }
        } catch (Exception ex) {
            System.out.println("Error bro:" + ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Kegagalan",
                    "Gagal dalam mengambil data User Group. "
                    + ex.toString()));
        }

    }

    public String getSelectedYear() {
        return selectedYear;
    }

    public void setSelectedYear(String selectedYear) {
        this.selectedYear = selectedYear;
    }

    public List<String> getYears() {
        return years;
    }

    public String getLabelYear() {
        return labelYear;
    }

    public void setLabelYear(String labelYear) {
        this.labelYear = labelYear;
    }

    public List<Pemda> getPemdas() {
        return pemdas;
    }

    public void setPemdas(List<Pemda> pemdas) {
        this.pemdas = pemdas;
    }

    public Pemda getSelectedPemda() {
        return selectedPemda;
    }

    public void setSelectedPemda(Pemda selectedPemda) {
        this.selectedPemda = selectedPemda;
    }

    public boolean isViewPemda() {
        return viewPemda;
    }

    public void setViewPemda(boolean viewPemda) {
        this.viewPemda = viewPemda;
    }
    
    public void insertPfk(){
        String s;
        if (pfk != null) {
            try {
                System.out.println("mulai mauinser");
                s = apbdServiceSession.inputPerhitunganFihakKetiga(pfk, (short)2);
                if(!s.trim().toLowerCase().startsWith("sukses"))
                    s = "Gagal input data PFK : " +s;
            } catch (SIKDServiceException ex) {
                Logger.getLogger(UploadPfkXmlMBean.class.getName()).log(Level.SEVERE, null, ex);
                s = " gagal input PFK " + ex.getMessage();
            }
            stats.add(s);
        }
        File ff = new File(fileName);
        if(ff.exists()) ff.delete();
    }
    
    public void parsingXML() throws Exception{
        try {
            JAXBContext jc= JAXBContext.newInstance(PerhitunganFihakKetiga_WS.class);
            Unmarshaller um = jc.createUnmarshaller();
            System.out.println("mulai parsing");
            JAXBElement<PerhitunganFihakKetiga_WS> pfkEL = um.unmarshal(new StreamSource(fileName), PerhitunganFihakKetiga_WS.class);
//            System.out.println("1");
            pfk = pfkEL.getValue();
//            System.out.println("2");
//            if(apbd !=null){
                String sat = pfk.getKodeSatker();
                System.out.println("satker "+ sat);
                if(!selectedPemda.getKodeSatker().trim().equals(sat)){
                    pfk = null;
                    throw new Exception("kode satker dalam file xml tidak sesuai untuk pemerintah daerah " + selectedPemda.getNamaPemda() );
                }
//            }
            stats.add("Sukses Parsing Data PFK ");
            stats.add("Mulai Input Data PFK ");
        } catch (JAXBException | SIKDServiceException ex) {
            Logger.getLogger(UploadPfkXmlMBean.class.getName()).log(Level.SEVERE, null, ex);
            throw new Exception("gagal parsing data PFK " + ex.getMessage());
        }
    }
    
    public void getObjectFromXML(){
        try {
            parsingXML();
        } catch (Exception ex) {
            Logger.getLogger(UploadPfkXmlMBean.class.getName()).log(Level.SEVERE, null, ex);
            stats.add(ex.getMessage());
        }
    }
    
    public void handleFileUploadChange() {
        stats.clear();
        pfk=null;
    }
    
    String fileName;
    @SuppressWarnings("ConvertToTryWithResources")
    public void handleFileUpload(FileUploadEvent e) {
        pfk = null;
        stats.clear();
        
        try {
        OutputStream outputStream;
        UploadedFile f = e.getFile();
        
            fileName = f.getFileName();
            InputStream inps = f.getInputstream();
            OutputStream outs = new FileOutputStream(fileName);
            copy(inps, outs);
            inps.close();
            outs.close();
            stats.add("Selesai Upload data PFK ");
            stats.add("Mulai Parsing data PFK ");
        }catch (IOException ex) {
            Logger.getLogger(UploadPfkXmlMBean.class.getName()).log(Level.SEVERE, null, ex.getMessage());
            stats.add("gagal upload File "+ ex.getMessage());
        }
    }

    public List<String> getStats() {
        return stats;
    }

    public void setStats(List<String> stats) {
        this.stats = stats;
    }

    public UploadFile getSelectedUploadFile() {
        return selectedUploadFile;
    }

    public void setSelectedUploadFile(UploadFile selectedUploadFile) {
        this.selectedUploadFile = selectedUploadFile;
    }

    public PerhitunganFihakKetiga_WS getPfk() {
        return pfk;
    }

    public void setPfk(PerhitunganFihakKetiga_WS pfk) {
        this.pfk = pfk;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
    
    public void onYearChange(){
        if( selectedPemda!= null) namaPemda = selectedPemda.getNamaPemda();
    }

    public String getNamaPemda() {
        return namaPemda;
    }

    public void setNamaPemda(String namaPemda) {
        this.namaPemda = namaPemda;
    }
    
    
    
    

}
