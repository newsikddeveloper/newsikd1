/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.managementdata.input;

import app.sikd.web.common.bean.ANeracaBackBean;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Nongky
 */
@ViewScoped
@ManagedBean(name = "inputNeracaBean")
public class InputNeracaBean extends ANeracaBackBean {

    @PostConstruct
    @Override
    protected void init() {
        super.init();
        setPage("/ManajemenData/InputData/neraca/neraca-content.xhtml");
    }

    @Override
    public void onChangeProcess() {

    }

    public List getData() {
        return null;
    }
}
