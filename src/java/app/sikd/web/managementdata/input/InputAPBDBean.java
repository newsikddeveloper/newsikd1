/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.managementdata.input;

import app.sikd.entity.Pemda;
import app.sikd.web.common.ASinglePageBackbean;
import app.sikd.web.common.bean.ABasicFilteredBackBean;
import app.sikd.web.common.bean.IPemdaFilteredBean;
import app.sikd.web.common.bean.IYearFilteredBean;
import app.sikd.web.common.filter.PemdaFilter;
import app.sikd.web.common.filter.YearFilter;
import app.sikd.web.common.listener.IFilterChangedListener;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Nongky
 */
@ViewScoped
@ManagedBean(name = "inputAPBDBean")
public class InputAPBDBean extends ABasicFilteredBackBean implements  IFilterChangedListener {


    private String dataCode;
    private String coaType;

    @PostConstruct
    @Override
    protected void init() {
        super.init();
        setPage("/ManajemenData/InputData/apbd/apbd-content.xhtml");
    }


    public String getDataCode() {
        return dataCode;
    }

    public void setDataCode(String dataCode) {
        this.dataCode = dataCode;
    }

    public String getCoaType() {
        return coaType;
    }

    public void setCoaType(String coaType) {
        this.coaType = coaType;
    }
    
    public List getData(){
        return null;
    }

    @Override
    protected void onChangeProcess() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
