/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.web.managementdata.pdf;

import app.sikd.entity.Pemda;
import app.sikd.entity.utilitas.UploadFile;
import app.sikd.office.ejb.session.UploadSessionBeanRemote;
import app.sikd.service.ServiceServerUtil;
import app.sikd.web.common.bean.IPemdaFilteredBean;
import app.sikd.web.common.bean.IYearFilteredBean;
import app.sikd.web.common.filter.PemdaFilter;
import app.sikd.web.common.filter.YearFilter;
import app.sikd.web.common.listener.IFilterChangedListener;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.naming.Context;

/**
 *
 * @author Dell
 */
@ManagedBean(name = "listDataPDFBean")
@ViewScoped
public class ListDataPDFBackBean implements Serializable, IPemdaFilteredBean, IYearFilteredBean, IFilterChangedListener {
    private PemdaFilter pemdaFilter;
    private YearFilter yearFilter;
    private Pemda pemda;
    private String year;
    
    private List<UploadFileObj> uploadFiles;
    private String labelYear;
    private String labelPemda;
    private UploadSessionBeanRemote uploadSession;

    @PostConstruct
    private void init() {
        pemdaFilter = new PemdaFilter();
        yearFilter = new YearFilter();
        
        try {
            uploadFiles = new ArrayList();
            Context ctxOffice = ServiceServerUtil.getOfficeContext();            
            uploadSession = (UploadSessionBeanRemote) ctxOffice.lookup("UploadSessionBean/remote");
            
            updateContentData();
                   
        } catch (Exception ex) {
            Logger.getLogger(ListDataPDFBackBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void onFilterChanged() {
        setLabelYear(getSelectedYear());
        updateContentData();
    }

    @Override
    public List<Pemda> getPemdas() {
        return pemdaFilter.getPemdas();
    }

    @Override
    public List<String> getYears() {
        return yearFilter.getYears();
    }

    @Override
    public void setSelectedPemda(Pemda _pemda) {
        pemda = _pemda;
    }

    @Override
    public Pemda getSelectedPemda() {
        return pemda;
    }

    @Override
    public void setSelectedYear(String _year) {
        year = _year;
    }

    @Override
    public String getSelectedYear() {
        return year;
    }
    
    public List<UploadFileObj> getUploadFiles() {
        return uploadFiles;
    }

    public void setUploadFiles(List<UploadFileObj> uploadFiles) {
        this.uploadFiles = uploadFiles;
    }
    
    public void updateContentData() {
        try {
            uploadFiles.clear();
            uploadFiles.add(new UploadFileObj("Ringkasan APBD Murni"));
            uploadFiles.add(new UploadFileObj("Ringkasan APBD Perubahan"));
            uploadFiles.add(new UploadFileObj("Laporan Realisasi Anggaran"));
            uploadFiles.add(new UploadFileObj("Daftar Transaksi Harian"));
            uploadFiles.add(new UploadFileObj("Neraca Semester 1"));
            uploadFiles.add(new UploadFileObj("Neraca Semester 2"));
            uploadFiles.add(new UploadFileObj("Arus Kas"));
            uploadFiles.add(new UploadFileObj("Perubahan Ekuitas"));
            uploadFiles.add(new UploadFileObj("Perubahan Sal"));
            uploadFiles.add(new UploadFileObj("Laporan Operasional Triwulan 1"));
            uploadFiles.add(new UploadFileObj("Laporan Operasional Triwulan 2"));
            uploadFiles.add(new UploadFileObj("Laporan Operasional Triwulan 3"));
            uploadFiles.add(new UploadFileObj("Laporan Operasional Triwulan 4"));
            uploadFiles.add(new UploadFileObj("Pinjaman Daerah"));
            uploadFiles.add(new UploadFileObj("Perhitungan Fihak Ketiga"));
            
            short tyear = 0;
            if (getSelectedYear() != null && !getSelectedYear().trim().equals("")) {
                tyear = Short.parseShort(getSelectedYear());
            }
            if (tyear > 0) {
                setLabelYear("TAHUN ANGGARAN " + tyear);
            }
            
           if(getSelectedPemda() != null) {            
            List<UploadFile> files = uploadSession.getAllUploadFiles(getSelectedPemda().getKodeSatker(), tyear);
            if( uploadFiles!=null && files!=null){
                for (UploadFile ff : files) {
                    setFile(ff);
                }
            }
           }
            
        } catch (Exception ex) {
            Logger.getLogger(ListDataPDFBackBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public String getLabelYear() {
        return labelYear;
    }

    public void setLabelYear(String labelYear) {
        this.labelYear = labelYear;
    }
    public String getLabelPemda() {
        return labelPemda;
    }

    public void setLabelPemda(String labelPemda) {
        this.labelPemda = labelPemda;
    }
    
    private void setFile(UploadFile ff){
        int ii= 0;
        boolean find = false;
        while(ii<uploadFiles.size() && find==false){
            if( uploadFiles.get(ii).getIkd().trim().equals(ff.getIkd().trim())){
                uploadFiles.get(ii).setId(ff.getId());
                uploadFiles.get(ii).setFilename(ff.getFilename().trim());
                uploadFiles.get(ii).setKdsatker(ff.getKdsatker().trim());
                uploadFiles.get(ii).setTahun(ff.getTahun());
                uploadFiles.get(ii).setTglupload(ff.getTglupload());                
                find=true;
            }
            ii++;
        }
    }
}

