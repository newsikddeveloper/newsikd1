/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.web.managementdata.pdf;

import java.io.Serializable;
import java.util.Date;
import java.util.StringTokenizer;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author sora
 */
public class UploadFileObj  implements Serializable{    
    private long id;    
    private UploadedFile file;
    private short tahun;
    private String ikd;
    private String kdsatker;
    private String filename;
    private String lastfilename;
    
    private Date tglupload;

    public UploadFileObj() {
    }

    public UploadFileObj(String ikd) {
        this.ikd = ikd;
    }

    public UploadFileObj(short tahun, String ikd, String kdsatker, String filename, Date tglupload) {
        this.tahun = tahun;
        this.ikd = ikd;
        this.kdsatker = kdsatker;
        this.filename = filename;
        this.tglupload = tglupload;
    }

    public UploadFileObj(long id, short tahun, String ikd, String kdsatker, String filename, Date tglupload) {
        this.id = id;
        this.tahun = tahun;
        this.ikd = ikd;
        this.kdsatker = kdsatker;
        this.filename = filename;
        this.tglupload = tglupload;
    }
    
    public long getId() {
        return id;
    }

    public String getKdsatker() {
        return kdsatker;
    }

    public String getFilename() {
        return filename;
    }

    public Date getTglupload() {
        return tglupload;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setKdsatker(String kdsatker) {
        this.kdsatker = kdsatker;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getLastfilename() {
        lastfilename = "";
        if(filename!=null && !filename.trim().equals("")){
            StringTokenizer tok = new StringTokenizer(filename, "\\");
            if(tok.countTokens()>0) {
                while(tok.hasMoreTokens()){
                    lastfilename=tok.nextToken();
                    System.out.println(lastfilename);
                    
                }
            }
        }
        return lastfilename;
    }

    public void setLastfilename(String lastfilename) {
        this.lastfilename = lastfilename;
    }

    public void setTglupload(Date tglupload) {
        this.tglupload = tglupload;
    }

    public short getTahun() {
        return tahun;
    }

    public void setTahun(short tahun) {
        this.tahun = tahun;
    }

    public String getIkd() {
        return ikd;
    }

    public void setIkd(String ikd) {
        this.ikd = ikd;
    }

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }
    
    
}
