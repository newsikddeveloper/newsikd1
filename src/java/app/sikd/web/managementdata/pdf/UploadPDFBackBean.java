/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.web.managementdata.pdf;

import app.sikd.entity.Pemda;
import app.sikd.entity.utilitas.UploadFile;
import app.sikd.office.ejb.session.UploadSessionBeanRemote;
import app.sikd.service.ServiceServerUtil;
import app.sikd.util.PropertiesLoader;
import app.sikd.web.common.bean.IPemdaFilteredBean;
import app.sikd.web.common.bean.IYearFilteredBean;
import app.sikd.web.common.filter.PemdaFilter;
import app.sikd.web.common.filter.YearFilter;
import app.sikd.web.common.listener.IFilterChangedListener;
import static com.google.common.io.ByteStreams.copy;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.naming.Context;
import javax.servlet.http.HttpServletResponse;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author Dell
 */
@ManagedBean(name = "uploadPDFBean")
@ViewScoped
public class UploadPDFBackBean implements Serializable, IPemdaFilteredBean, IYearFilteredBean, IFilterChangedListener {
    private PemdaFilter pemdaFilter;
    private YearFilter yearFilter;
    private Pemda pemda;
    private String year;
    
    private List<UploadFileObj> uploadFiles;
    private String labelYear;
    private String labelPemda;
    private String selectedIKD;
    
    private String uploadPath;
    private UploadSessionBeanRemote uploadSession;

    @PostConstruct
    private void init() {
        pemdaFilter = new PemdaFilter();
        yearFilter = new YearFilter();
        
        try {
            uploadFiles = new ArrayList();
            Context ctxOffice = ServiceServerUtil.getOfficeContext();            
            uploadPath = ((Properties)PropertiesLoader.loadProperties("sikd.properties")).getProperty("uploadPath");
            uploadSession = (UploadSessionBeanRemote) ctxOffice.lookup("UploadSessionBean/remote");
            
            updateContentData();
                   
        } catch (Exception ex) {
            Logger.getLogger(ListDataPDFBackBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void onFilterChanged() {
        setLabelYear(getSelectedYear());
        updateContentData();
    }

    @Override
    public List<Pemda> getPemdas() {
        return pemdaFilter.getPemdas();
    }

    @Override
    public List<String> getYears() {
        return yearFilter.getYears();
    }

    @Override
    public void setSelectedPemda(Pemda _pemda) {
        pemda = _pemda;
    }

    @Override
    public Pemda getSelectedPemda() {
        return pemda;
    }

    @Override
    public void setSelectedYear(String _year) {
        year = _year;
    }

    @Override
    public String getSelectedYear() {
        return year;
    }
    
    public List<UploadFileObj> getUploadFiles() {
        return uploadFiles;
    }

    public void setUploadFiles(List<UploadFileObj> uploadFiles) {
        this.uploadFiles = uploadFiles;
    }
    
    public void updateContentData() {
        try {
            uploadFiles.clear();
            uploadFiles.add(new UploadFileObj("Ringkasan APBD Murni"));
            uploadFiles.add(new UploadFileObj("Ringkasan APBD Perubahan"));
            uploadFiles.add(new UploadFileObj("Laporan Realisasi Anggaran"));
            uploadFiles.add(new UploadFileObj("Daftar Transaksi Harian"));
            uploadFiles.add(new UploadFileObj("Neraca Semester 1"));
            uploadFiles.add(new UploadFileObj("Neraca Semester 2"));
            uploadFiles.add(new UploadFileObj("Arus Kas"));
            uploadFiles.add(new UploadFileObj("Perubahan Ekuitas"));
            uploadFiles.add(new UploadFileObj("Perubahan Sal"));
            uploadFiles.add(new UploadFileObj("Laporan Operasional Triwulan 1"));
            uploadFiles.add(new UploadFileObj("Laporan Operasional Triwulan 2"));
            uploadFiles.add(new UploadFileObj("Laporan Operasional Triwulan 3"));
            uploadFiles.add(new UploadFileObj("Laporan Operasional Triwulan 4"));
            uploadFiles.add(new UploadFileObj("Pinjaman Daerah"));
            uploadFiles.add(new UploadFileObj("Perhitungan Fihak Ketiga"));
            
            short tyear = 0;
            if (getSelectedYear() != null && !getSelectedYear().trim().equals("")) {
                tyear = Short.parseShort(getSelectedYear());
            }
            if (tyear > 0) {
                setLabelYear("TAHUN ANGGARAN " + tyear);
            }
            
           if(getSelectedPemda() != null) {            
            List<UploadFile> files = uploadSession.getAllUploadFiles(getSelectedPemda().getKodeSatker(), tyear);
            if( uploadFiles!=null && files!=null){
                for (UploadFile ff : files) {
                    setFile(ff);
                }
            }
           }
            
        } catch (Exception ex) {
            Logger.getLogger(ListDataPDFBackBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public String getLabelYear() {
        return labelYear;
    }

    public void setLabelYear(String labelYear) {
        this.labelYear = labelYear;
    }
    public String getLabelPemda() {
        return labelPemda;
    }

    public void setLabelPemda(String labelPemda) {
        this.labelPemda = labelPemda;
    }
    
    public void onShowDialog(String ikd) {
        this.selectedIKD = ikd;
        System.out.println("klik");
    }
       
    private void setFile(UploadFile ff){
        int ii= 0;
        boolean find = false;
        while(ii<uploadFiles.size() && find==false){
            if( uploadFiles.get(ii).getIkd().trim().equals(ff.getIkd().trim())){
                uploadFiles.get(ii).setId(ff.getId());
                uploadFiles.get(ii).setFilename(ff.getFilename().trim());
                uploadFiles.get(ii).setKdsatker(ff.getKdsatker().trim());
                uploadFiles.get(ii).setTahun(ff.getTahun());
                uploadFiles.get(ii).setTglupload(ff.getTglupload());                
                find=true;
            }
            ii++;
        }
    }
    
    @SuppressWarnings("ConvertToTryWithResources")
    public void handleFileUpload(FileUploadEvent e) {
        try {
            short yy = 0;
            if (getSelectedYear() != null && !getSelectedYear().trim().equals("")) {
                yy = Short.parseShort(getSelectedYear());
            }

            OutputStream outputStream;
            UploadedFile f = e.getFile();
            String fileName = uploadPath + System.currentTimeMillis() + f.getFileName();
            InputStream inps = f.getInputstream();
            OutputStream outs = new FileOutputStream(fileName);
            copy(inps, outs);
            inps.close();
            outs.close();
            UploadFile uf = new UploadFile(yy, selectedIKD, getSelectedPemda().getKodeSatker(), fileName, new Date());
            uploadSession.addUploadFile(uf);
            selectedIKD = null;
            updateContentData();
        } catch (IOException ex) {
            Logger.getLogger(ListDataPDFBackBean.class.getName()).log(Level.SEVERE, null, ex.getMessage());
        } catch (Exception ex) {
            Logger.getLogger(ListDataPDFBackBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void download(String fileName) throws IOException {
        File file = new File(fileName);
        BufferedInputStream inp = null;
        BufferedOutputStream out = null;
        FacesContext fcontext = FacesContext.getCurrentInstance();
        HttpServletResponse response = (HttpServletResponse) fcontext.getExternalContext().getResponse();
        try{
        inp = new BufferedInputStream(new FileInputStream(file),10240);
        
        
        response.reset();
        response.setHeader("Content-Length", String.valueOf(file.length()));
        response.setHeader("Content-Disposition", "inline;filename=\""+fileName+"\"");
        
        out = new BufferedOutputStream(response.getOutputStream(), 10240);
        byte[] buffer = new byte[10240];
        int length;
        while((length=inp.read(buffer))>0){
            out.write(buffer, 0, length);
        }
        out.flush();
        }
        finally{
            inp.close();
            out.close();
        }
        fcontext.responseComplete();
                
    }
}

