/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web;

import java.io.Serializable;

/**
 *
 * @author sora
 */
public class Tes implements Serializable{
    private String nama;
    private String type;
    private String icon;

    public Tes(String nama, String type, String icon) {
        this.nama = nama;
        this.type = type;
        this.icon = icon;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return nama;
    }
    
    
    
}
